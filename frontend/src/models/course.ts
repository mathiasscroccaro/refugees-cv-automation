export type Course = {
  id?: number;
  refugeeId?: number;
  name: string;
  year: number;
  institution: string;
  supportedBy: string;
  createdAt?: Date;
  updatedAt?: Date;
};
