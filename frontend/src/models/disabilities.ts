export type Disabilities = {
  id?: number;
  whichDisabilities: string;
  disabilitiesMedicalReport: string;
  createdAt?: Date;
  updatedAt?: Date;
};
