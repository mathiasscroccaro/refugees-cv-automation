export type Role = {
  id?: number;
  roleName: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
};
