export enum EducationLevel {
  UNDERGRAD = "Superior completo",
  INCOMPLETD_UNDERGRAD = "Superior incompleto",
  TECNICIAN_HIGH_SCHOOL = "Ensino médio técnico",
  HIGH_SCHOOL = "Ensino médio completo",
  INCOMPLETED_HIGH_SCHOOL = "Ensino médio incompleto",
  ELEMENTARY_SCHOOL = "Ensino fundamental",
  INCOMPLETED_ELEMENTARY_SCHOOL = "Ensino fundamental incompleto",
}

export type Education = {
  id?: number;
  refugeeId?: number;
  level: EducationLevel;
  name?: string;
  institution: string;
  year: number;
  createdAt?: Date;
  updatedAt?: Date;
};
