import { Course } from "./course";
import { Education } from "./education";
import { OwnBusiness } from "./own_business";
import { ProfessionalExperience } from "./professional_experience";
import { Refugee } from "./refugee";

export enum FormEditingState {
  INCOMPLETE,
  COMPLETE,
}

type FormState<FieldType> = {
  data: FieldType;
  editingState: FormEditingState;
};

type CompleteFormState = {
  basicInfo: FormState<Refugee>;
  professionalExperience: FormState<ProfessionalExperience>[];
  couses: FormState<Course>[];
  educations: FormState<Education>[];
  ownBusiness: FormState<OwnBusiness>[];
};
