import request from "supertest";
import { app } from "index";
import { GetDatabaseUseCase } from "use_cases/databases";
import { Role } from "models/role";
import { GetAuthenticatedUserUseCase } from "use_cases/authentication";
import { User } from "models/user";
import { CreateOrUpdateRoleUseCase } from "use_cases/roles";

describe("When accessing privates endpoints without proper token", () => {
    beforeEach(async () => {
      const db = new GetDatabaseUseCase().execute();
      await db.sync();
    });
  
    it("GET /api/roles", async () => {
      await request(app)
        .get("/api/roles")
        .expect(401)
        .expect("Content-Type", /json/);
    });
  
    it("POST /api/roles", async () => {
      await request(app)
        .post("/api/roles")
        .expect(401)
        .expect("Content-Type", /json/);
    });
  
    it("PATCH /api/roles", async () => {
      await request(app)
        .patch("/api/roles")
        .expect(401)
        .expect("Content-Type", /json/);
    });
  
    it("DELETE /api/roles", async () => {
      await request(app)
        .delete("/api/roles/1")
        .expect(401)
        .expect("Content-Type", /json/);
    });
  });

  describe("When accessing private endpoints", () => {
    let role: Role;
    let token: string = "fakeToken";
  
    beforeEach(async () => {
      const db = new GetDatabaseUseCase().execute();
      await db.sync({ force: true });
  
      jest
        .spyOn(GetAuthenticatedUserUseCase.prototype, "execute")
        .mockImplementation((): Promise<User> => {
          return Promise.resolve({
            email: "test@test.com",
            password: "xxx",
            isAdmin: true,
          } as User);
        });
  
      const roleToBeCreated: Role = {
        roleName: 'software engineer',
        description: 'make softwares'
      };
  
      role = await new CreateOrUpdateRoleUseCase(db).execute(roleToBeCreated);
    });
  
    it("GET /api/roles", async () => {
      await request(app)
        .get("/api/roles")
        .set({ "x-access-token": token })
        .expect(200)
        .expect("Content-Type", /json/)
        .then(async (response) => {
          expect(response.body).toBeTruthy();
          expect(response.body).toEqual(
            expect.arrayContaining([
              expect.objectContaining({ roleName: role.roleName, description: role.description }),
            ])
          );
        });
    });
  
    it('POST /api/roles', async () => {
      const newRole: Role = {
        roleName: 'backend engineer'
      }
      await request(app)
        .post("/api/roles")
        .set({ "x-access-token": token })
        .send(newRole)
        .expect(201)
        .expect("Content-Type", /json/)
        .then(async (response) => {
          expect(response.body).toBeTruthy();
          expect(response.body).toEqual(
            expect.objectContaining({
              roleName: newRole.roleName
            })
          );
        });
    });
  
    it('PATCH /api/roles', async () => {
      const newRole: Role = {
        roleName: 'software engineer',
        description: 'makes good software'
      }
      await request(app)
        .patch("/api/roles")
        .set({ "x-access-token": token })
        .send(newRole)
        .expect(201)
        .expect("Content-Type", /json/)
        .then(async (response) => {
          expect(response.body).toBeTruthy();
          expect(response.body).toEqual(
            expect.objectContaining({
              roleName: newRole.roleName,
              description: newRole.description
            })
          );
        });
    });
  
    it('DELETE /api/roles/id', async () => {
      await request(app)
        .delete("/api/roles/1")
        .set({ "x-access-token": token })
        .expect(204);
    });
  });