import request from "supertest";
import { app } from "index";

import {
  CreateUserUseCase,
  GetUserTokenByEmailAndPasswordUseCase,
} from "use_cases/users";
import { GetDatabaseUseCase } from "use_cases/databases";
import { User } from "models/user";

function compareUsers(userOne: User, userTwo: User) {
  expect(userOne.email).toEqual(userTwo.email);
  expect(userOne.id).toEqual(userTwo.id);
  expect(userOne.isAdmin).toEqual(userTwo.isAdmin);
}

describe("When accessing public endpoints", () => {
  beforeEach(async () => {
    const db = new GetDatabaseUseCase().execute();
    await db.sync();
    await new CreateUserUseCase(db).execute({
      email: "test@test.com",
      password: "test",
    });
  });
  it("POST /api/users/token", async () => {
    await request(app)
      .post("/api/users/token")
      .send({ email: "test@test.com", password: "test" })
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body.token).toBeTruthy();
        expect(response.body.token.length).toBeGreaterThan(32);
      });
  });
});

describe("When accessing private endpoints", () => {
  let token: string | null;
  let user: User;

  beforeAll(async () => {
    const db = new GetDatabaseUseCase().execute();
    await db.sync({ force: true });

    const email: string = "test@test.com";
    const password: string = "testPassword";

    user = await new CreateUserUseCase(db).execute({ email, password });
    token = await new GetUserTokenByEmailAndPasswordUseCase(db).execute(
      email,
      password
    );
  });

  it("POST /api/users/me", async () => {
    await request(app)
      .get("/api/users/me")
      .set({ "x-access-token": token })
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();

        compareUsers(response.body, user);
      });
  });
});

describe("When accessing admin endpoints", () => {
  let adminToken: string | null;
  let nonAdminToken: string | null;

  let adminUser: User;
  let nonAdminUser: User;

  beforeAll(async () => {
    const db = new GetDatabaseUseCase().execute();
    await db.sync({ force: true });

    const signInAdminUser: User = {
      email: "admin@test.com",
      password: "adminPassword",
      isAdmin: true,
    };

    const signInNonAdminUser: User = {
      email: "test@test.com",
      password: "testPassword",
      isAdmin: false,
    };

    adminUser = await new CreateUserUseCase(db).execute(signInAdminUser);
    nonAdminUser = await new CreateUserUseCase(db).execute(signInNonAdminUser);
    adminToken = await new GetUserTokenByEmailAndPasswordUseCase(db).execute(
      signInAdminUser.email,
      signInAdminUser.password
    );
    nonAdminToken = await new GetUserTokenByEmailAndPasswordUseCase(db).execute(
      signInNonAdminUser.email,
      signInNonAdminUser.password
    );
  });

  it("GET /api/users", async () => {
    await request(app)
      .get("/api/users")
      .set({ "x-access-token": adminToken })
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body.length).toBe(2);

        compareUsers(response.body[0], adminUser);
        compareUsers(response.body[1], nonAdminUser);
      });
  });

  it("POST /api/users", async () => {
    const userToBeCreated: User = {
      email: "new@test.com",
      password: "newPassword",
      isAdmin: false,
    };

    await request(app)
      .post("/api/users")
      .set({ "x-access-token": adminToken })
      .send(userToBeCreated)
      .expect(201)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();

        compareUsers({ id: 3, ...userToBeCreated } as User, response.body);
      });
  });

  it("POST /api/users with missing parameters", async () => {
    const userToBeCreated: Partial<User> = {
      email: "new@test.com",
      isAdmin: false,
    };

    await request(app)
      .post("/api/users")
      .set({ "x-access-token": adminToken })
      .send(userToBeCreated)
      .expect(400)
      .expect("Content-Type", /json/);
  });

  it("PATCH /api/users/:id", async () => {
    const userToBeUpdated: User = {
      id: 2,
      email: "userUpdated@test.com",
      password: "newPassword",
      isAdmin: false,
    };

    await request(app)
      .patch(`/api/users/${userToBeUpdated.id}`)
      .set({ "x-access-token": adminToken })
      .send(userToBeUpdated)
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();

        compareUsers(userToBeUpdated, response.body);
      });
  });

  it("PATCH /api/users/:id with non existance id", async () => {
    const userToBeUpdated: User = {
      id: 100,
      email: "userUpdated@test.com",
      password: "newPassword",
      isAdmin: false,
    };

    await request(app)
      .patch(`/api/users/${userToBeUpdated.id}`)
      .set({ "x-access-token": adminToken })
      .send(userToBeUpdated)
      .expect(404)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
      });
  });

  it("DELETE /api/users/:id after an POST /users", async () => {
    let id: number = -1;

    const user: User = {
      email: "tempUser@test.com",
      password: "newPassword",
      isAdmin: false,
    };

    await request(app)
      .post("/api/users")
      .set({ "x-access-token": adminToken })
      .send(user)
      .expect(201)
      .then((response) => {
        id = response.body.id;
        expect(id).toBeTruthy();
      });

    await request(app)
      .delete(`/api/users/${id}`)
      .set({ "x-access-token": adminToken })
      .expect(204);
  });

  it("DELETE /api/users/:id by non Admin user", async () => {
    const userIdToBeDeleted: number = 1;

    await request(app)
      .delete(`/api/users/${userIdToBeDeleted}`)
      .set({ "x-access-token": nonAdminToken })
      .expect(403);
  });

  it("DELETE /api/users/:id with non existance id", async () => {
    const userIdToBeDeleted: number = 100;

    await request(app)
      .delete(`/api/users/${userIdToBeDeleted}`)
      .set({ "x-access-token": adminToken })
      .expect(404);
  });
});
