import { Gender, Refugee, User } from "@/models";
import { GetAuthenticatedUserUseCase } from "@/use_cases/authentication";
import { CreateRefugeeUseCase } from "@/use_cases/refugees";
import { app } from "index";
import request from "supertest";
import { GetDatabaseUseCase } from "use_cases/databases";

describe("When accessing privates endpoints without proper token", () => {
  beforeEach(async () => {
    const db = new GetDatabaseUseCase().execute();
    await db.sync();
  });

  it("POST /api/search", async () => {
    await request(app)
      .post("/api/search")
      .expect(401)
      .expect("Content-Type", /json/);
  });
});

describe("When accessing private endpoints", () => {
  let refugee: Refugee;
  let token: string = "fakeToken";

  beforeEach(async () => {
    const db = new GetDatabaseUseCase().execute();
    await db.sync({ force: true });

    jest
      .spyOn(GetAuthenticatedUserUseCase.prototype, "execute")
      .mockImplementation((): Promise<User> => {
        return Promise.resolve({
          email: "test@test.com",
          password: "xxx",
          isAdmin: true,
        } as User);
      });

    const refugeeToBeCreated: Refugee = {
      cpf: "00011122233",
      name: "Test Name",
      dateOfBirth: new Date(),
      gender: Gender.M,
    };

    refugee = await new CreateRefugeeUseCase(db).execute(refugeeToBeCreated);
  });

  it("POST /api/search", async () => {
    await request(app)
      .post("/api/search")
      .set({ "x-access-token": token })
      .send({ query: "Name" })
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({ cpf: refugee.cpf, name: refugee.name }),
          ])
        );
      });
    await request(app)
      .post("/api/search")
      .set({ "x-access-token": token })
      .send({ query: "Test" })
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({ cpf: refugee.cpf, name: refugee.name }),
          ])
        );
      });
    await request(app)
      .post("/api/search")
      .set({ "x-access-token": token })
      .send({ query: "Tes" })
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({ cpf: refugee.cpf, name: refugee.name }),
          ])
        );
      });
  });
  it('POST /api/search should not find anything', async () => {
    await request(app)
      .post("/api/search")
      .set({ "x-access-token": token })
      .send({ query: "X" })
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual([]);
      });
  });
});
