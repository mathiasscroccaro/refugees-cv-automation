import { Gender, Refugee } from "models/refugee";
import { User } from "models/user";
import request from "supertest";
import { GetAuthenticatedUserUseCase } from "use_cases/authentication";
import { CreateRefugeeUseCase } from "use_cases/refugees";
import { app } from "index";

import { GetDatabaseUseCase } from "../../use_cases/databases";

describe("When accessing privates endpoints without proper token", () => {
  beforeEach(async () => {
    const db = new GetDatabaseUseCase().execute();
    await db.sync();
  });

  it("GET /api/refugees", async () => {
    await request(app)
      .get("/api/refugees")
      .expect(401)
      .expect("Content-Type", /json/);
  });

  it("POST /api/refugees", async () => {
    await request(app)
      .post("/api/refugees")
      .expect(401)
      .expect("Content-Type", /json/);
  });

  it("PATCH /api/refugees", async () => {
    await request(app)
      .patch("/api/refugees/1")
      .expect(401)
      .expect("Content-Type", /json/);
  });

  it("DELETE /api/refugees", async () => {
    await request(app)
      .delete("/api/refugees/1")
      .expect(401)
      .expect("Content-Type", /json/);
  });
});

describe("When accessing private endpoints", () => {
  let refugee: Refugee;
  let token: string = "fakeToken";

  beforeEach(async () => {
    const db = new GetDatabaseUseCase().execute();
    await db.sync({ force: true });

    jest
      .spyOn(GetAuthenticatedUserUseCase.prototype, "execute")
      .mockImplementation((): Promise<User> => {
        return Promise.resolve({
          email: "test@test.com",
          password: "xxx",
          isAdmin: true,
        } as User);
      });

    const refugeeToBeCreated: Refugee = {
      cpf: "00011122233",
      name: "Test Name",
      dateOfBirth: new Date(),
      gender: Gender.M,
    };

    refugee = await new CreateRefugeeUseCase(db).execute(refugeeToBeCreated);
  });

  it("GET /api/refugees", async () => {
    await request(app)
      .get("/api/refugees")
      .set({ "x-access-token": token })
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({ cpf: refugee.cpf, name: refugee.name }),
          ])
        );
      });
  });

  it("GET /api/refugees with non Admin user", async () => {
    jest
      .spyOn(GetAuthenticatedUserUseCase.prototype, "execute")
      .mockImplementation((): Promise<User> => {
        return Promise.resolve({
          email: "test@test.com",
          password: "xxx",
          isAdmin: false,
        } as User);
      });

    await request(app)
      .get("/api/refugees")
      .set({ "x-access-token": token })
      .expect(403)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.objectContaining({
            auth: true,
            message: "User test@test.com does not have admin permission",
          })
        );
      });
  });

  it('GET /api/refugees/:cpf', async () => {
    await request(app)
      .get("/api/refugees/00011122233")
      .set({ "x-access-token": token })
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.objectContaining({
            name: refugee.name,
            cpf: refugee.cpf
          })
        );
      });
  });

  it('GET /api/refugees/:cpf with non existing CPF', async () => {
    await request(app)
      .get("/api/refugees/111")
      .set({ "x-access-token": token })
      .expect(404)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.objectContaining({
            message: "Refugee CPF = 111 doesn't exist",
            auth: true
          })
        );
      });
  });

  it('POST /api/refugees', async () => {
    const newRefugee: Refugee = {
      name: 'New Test Refugee',
      cpf: '11122233344',
      dateOfBirth: new Date(),
      gender: Gender.F
    }
    await request(app)
      .post("/api/refugees")
      .set({ "x-access-token": token })
      .send(newRefugee)
      .expect(201)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.objectContaining({
            cpf: newRefugee.cpf,
            name: newRefugee.name
          })
        );
      });
  });

  it('PATCH /api/refugees/:cpf', async () => {
    const newRefugee: Partial<Refugee> = {
      name: 'New Test Refugee',
      dateOfBirth: new Date(),
      gender: Gender.F
    }
    await request(app)
      .patch("/api/refugees/00011122233")
      .set({ "x-access-token": token })
      .send(newRefugee)
      .expect(200)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.objectContaining({
            name: newRefugee.name,
            gender: newRefugee.gender
          })
        );
      });
  });

  it('PATCH /api/refugees/:cpf with non existing CPF', async () => {
    const newRefugee: Partial<Refugee> = {
      name: 'New Test Refugee',
    }
    await request(app)
      .patch("/api/refugees/111")
      .set({ "x-access-token": token })
      .send(newRefugee)
      .expect(404)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.objectContaining({
            auth: true,
            message: "Refugee with CPF = 111 doesn't exist"
          })
        );
      });
  });

  it('DELETE /api/refugees/cpf', async () => {
    await request(app)
      .delete("/api/refugees/00011122233")
      .set({ "x-access-token": token })
      .expect(204);
  });

  it('DELETE /api/refugees/cpf', async () => {
    await request(app)
      .delete("/api/refugees/000")
      .set({ "x-access-token": token })
      .expect(404)
      .expect("Content-Type", /json/)
      .then(async (response) => {
        expect(response.body).toBeTruthy();
        expect(response.body).toEqual(
          expect.objectContaining({auth: true, message: "Refugee CPF = 000 doesn't exist"})
        );
      });
  })
});
