import express, { Express } from "express";
import { APIRouter } from "./routes";

import * as dotenv from "dotenv";
dotenv.config();

const app: Express = express();
const port: number = Number(process.env.PORT) || 3000;

app.use(express.json());

app.use("/api", APIRouter);

function startAPIServer() {
  const server = app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at https://localhost:${port}`);
  });
  return server;
}

export { app, startAPIServer };
