import { ErrorBase } from "models/error";
import { GetDatabaseUseCase } from "@/use_cases/databases";
import { SearchRefugeeByNameOrCPF } from "use_cases/search";
import { Request, Response } from "express";

export async function searchRefugeeController(req: Request, res: Response) {
    try {
      const db = new GetDatabaseUseCase().execute();
      const refugeeSearchList = await new SearchRefugeeByNameOrCPF(db).execute(req.body.query)
      res.send(refugeeSearchList);
    } catch (error) {
      if (error instanceof ErrorBase) {
        res.status(error.status_code);
        res.send(error.getResponse());
      } else {
        res.sendStatus(500);
      }
    }
  }