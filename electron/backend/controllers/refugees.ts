import express, { Router, Request, Response, NextFunction } from "express";
import { ErrorBase } from "models/error";
import { GetDatabaseUseCase } from "use_cases/databases";
import { check, validationResult } from "express-validator";
import {
  CreateRefugeeUseCase,
  DeleteRefugeeByCPFUseCase,
  GetAllRefugeesUseCase,
  GetRefugeeByCPFUseCase,
  UpdateRefugeeByCPFUseCase,
} from "use_cases/refugees";

const router: Router = express.Router();

export async function getAllRefugeesController(req: Request, res: Response) {
  const db = new GetDatabaseUseCase().execute();
  const refugeeList = await new GetAllRefugeesUseCase(db).execute();
  res.send(refugeeList);
}

export async function getRefugeeController(req: Request, res: Response) {
  try {
    const db = new GetDatabaseUseCase().execute();
    const refugee = await new GetRefugeeByCPFUseCase(db).execute(
      req.params.cpf
    );
    res.status(200);
    res.send(refugee);
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}

export async function createRefugeeController(req: Request, res: Response) {
  try {
    const db = new GetDatabaseUseCase().execute();
    const refugee = await new CreateRefugeeUseCase(db).execute(req.body);
    res.status(201);
    res.send(refugee);
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}

export async function updateRefugeeController(req: Request, res: Response) {
  try {
    const db = new GetDatabaseUseCase().execute();
    const refugee = await new UpdateRefugeeByCPFUseCase(db).execute(
      req.params.cpf,
      req.body
    );
    res.status(200);
    res.send(refugee);
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}

export async function deleteRefugeeController(req: Request, res: Response) {
  try {
    const cpf: string = req.params.cpf;
    const db = new GetDatabaseUseCase().execute();
    await new DeleteRefugeeByCPFUseCase(db).execute(cpf);
    res.sendStatus(204);
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}

const commonValidation = [check(["name", "cpf"]).isString()];

const optionalValidation = [
  check("id").isNumeric(),
  check("address").isString(),
  check("email").isEmail(),
  check("phone").isString(),
];

export const validateUpdateRefugee = [
  ...optionalValidation.map((value) => value.optional()),
  ...commonValidation.map((value) => value.optional()),
  (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(400).json({ errors: errors.array() });
    next();
  },
];

export const validateCreateRefugee = [
  ...commonValidation,
  (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(400).json({ errors: errors.array() });
    next();
  },
];
