import { Request, Response, NextFunction } from "express";
import {
  CreateUserUseCase,
  DeleteUserByIdUseCase,
  GetAllUsersUseCase,
  GetUserTokenByEmailAndPasswordUseCase,
  UpdateUserByIdUseCase,
} from "use_cases/users";
import { GetDatabaseUseCase } from "use_cases/databases";
import { Error401, ErrorBase } from "models/error";
import { check, validationResult } from "express-validator";

export async function getTokenController(req: Request, res: Response) {
  if (!req.body.email || !req.body.password)
    res.sendStatus(401).json(new Error401("user and password are missing"));

  const db = new GetDatabaseUseCase().execute();

  const token = await new GetUserTokenByEmailAndPasswordUseCase(db).execute(
    req.body.email,
    req.body.password
  );

  res.send({
    token,
  });
}

export async function getMeController(req: Request, res: Response) {
  res.send(req.user);
}

export async function getAllUsersController(req: Request, res: Response) {
  const db = new GetDatabaseUseCase().execute();
  const userList = await new GetAllUsersUseCase(db).execute();
  res.send(userList);
}

export async function createUserController(req: Request, res: Response) {
  try {
    const db = new GetDatabaseUseCase().execute();
    const user = await new CreateUserUseCase(db).execute(req.body);

    res.status(201);
    res.send(user);
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}

export async function updateUserController(req: Request, res: Response) {
  try {
    const id: number = Number(req.params.id);
    const db = new GetDatabaseUseCase().execute();
    const user = await new UpdateUserByIdUseCase(db).execute(id, req.body);

    res.status(200);
    res.send(user);
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}

export async function deleteUserController(req: Request, res: Response) {
  try {
    const id: number = Number(req.params.id);
    const db = new GetDatabaseUseCase().execute();
    await new DeleteUserByIdUseCase(db).execute(id);
    res.sendStatus(204);
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}

export const validateUser = [
  check("email").isEmail(),
  check("password").notEmpty().isLength({ min: 8 }),
  (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(400).json({ errors: errors.array() });
    next();
  },
];
