import { Request, Response } from "express";
import { ErrorBase } from "models/error";
import { GetDatabaseUseCase } from "use_cases/databases";
import {
  CreateOrUpdateRoleUseCase,
  DeleteRoleByIDUseCase,
  GetAllRolesUseCase,
} from "use_cases/roles";

export async function getAllRolesController(req: Request, res: Response) {
  try {
    const db = new GetDatabaseUseCase().execute();
    const refugeeList = await new GetAllRolesUseCase(db).execute();
    res.send(refugeeList);
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}

export async function deleleRoleController(req: Request, res: Response) {
  try {
    const id: number = Number(req.params.id);
    const db = new GetDatabaseUseCase().execute();
    await new DeleteRoleByIDUseCase(db).execute(id);
    res.sendStatus(204);
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}

export async function createOrUpdateRoleController(
  req: Request,
  res: Response
) {
  try {
    const db = new GetDatabaseUseCase().execute();
    const role = await new CreateOrUpdateRoleUseCase(db).execute(req.body);
    res.status(201);
    res.send(role);
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}
