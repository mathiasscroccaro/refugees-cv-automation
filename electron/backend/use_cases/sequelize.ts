import { Sequelize } from "sequelize";

export class GetSequelizeInstanceUseCase {
  execute(): Sequelize {
    if (process.env.NODE_ENV === "test") {
      return new Sequelize("sqlite::memory:", {
        logging: false,
        omitNull: true,
      });
    } else {
      return new Sequelize({
        dialect: "sqlite",
        storage: "./database.sqlite",
        logging: false,
        omitNull: true,
      });
    }
  }
}
