import { IDataBase } from "database/interfaces";
import { Error404 } from "models/error";
import { Role } from "models/role";

export class CreateOrUpdateRoleUseCase {
  database: IDataBase;
  constructor(database: IDataBase) {
    this.database = database;
  }
  async execute(role: Role) {
    return await this.database.findUpdateOrCreateRole(role);
  }
}

export class DeleteRoleByIDUseCase {
  database: IDataBase;
  constructor(database: IDataBase) {
    this.database = database;
  }
  async execute(id: number) {
    const numberOfEntriesDeleted = await this.database.deleteRoleById(id);
    if (numberOfEntriesDeleted === 0)
      throw new Error404(`Role with ID = ${id} cannot be found`);
  }
}

export class GetAllRolesUseCase {
  database: IDataBase;
  constructor(database: IDataBase) {
    this.database = database;
  }
  async execute(): Promise<Role[]> {
    return await this.database.getAllRoles();
  }
}
