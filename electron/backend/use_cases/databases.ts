import { MemoryDB } from "database/memory_db";
import { IDataBase } from "database/interfaces";
import { SQLiteDB } from "database/sqlite_db";

export class GetDatabaseUseCase {
  execute(): IDataBase {
    if (process.env.NODE_ENV === "test") {
      return new MemoryDB();
    } else {
      return new SQLiteDB();
    }
  }
}
