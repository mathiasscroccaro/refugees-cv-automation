import { IDataBase } from "database/interfaces";
import { RefugeeSearch } from "models";

export class SearchRefugeeByNameOrCPF {
    database: IDataBase;
    
    constructor(database: IDataBase) {
        this.database = database;
    }

    async execute(query: string): Promise<RefugeeSearch[]> {
        if (query.length < 3)
            return [];
        const CFPQueryResult = await this.database.searchRefugeeByCPF(query);
        if (CFPQueryResult.length)
            return CFPQueryResult; 
        else
            return await this.database.searchRefugeeByName(query);
    }
}