import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

import { IDataBase } from "../database/interfaces";
import { User, Token } from "../models/user";
import { Error400, Error404 } from "../models/error";

import * as dotenv from 'dotenv';
dotenv.config();

const secretKey: string = process.env.TOKEN_SECRET_KEY || 'secret';

export class GetUserTokenByEmailAndPasswordUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  generateUserToken(id: number): Token {
    return jwt.sign({ id }, secretKey);
  }

  isValidPassword(hashedPassword: string, password: string): boolean {
    return bcrypt.compareSync(password, hashedPassword);
  }

  async execute(email: string, password: string): Promise<Token | null> {
    try {
      const user = await this.database.getUserByEmail(email);
      if (!user)
        return Promise.resolve(null);
      if (!user) return Promise.resolve(null);

      if (this.isValidPassword(user.password, password) && user.id)
        return Promise.resolve(this.generateUserToken(user.id));
      else return Promise.resolve(null);
    } catch(error) {
        throw(error);
    }
  }
}


export class GetUserByTokenUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  execute(token: Token): Promise<User | null> {
    try {
      const decoded = jwt.verify(token, secretKey) as any;
      return this.database.getUserById(decoded.id);
    } catch (error) {
      throw(error)
    }
  }
}


export class CreateUserUseCase {
    database: IDataBase;

    constructor(database: IDataBase) {
        this.database = database;
    }

    async execute(user: User) : Promise<User> {
        const userToBeCreated = await this.database.createUser(user);
        if(!userToBeCreated)
            throw new Error400(`Refugee with email = ${user.email} cannot be duplicated`);
        return userToBeCreated;
    }
}


export class UpdateUserByIdUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  async execute(id: number, user: Partial<User>) : Promise<User | null> {
    const updatedUser = await this.database.modifyUserById(id, user);
    if(updatedUser)
      return updatedUser;
    else 
      throw new Error404(`User ID = ${id} doesn't exist`);
  }
}


export class DeleteUserByIdUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  async execute(id: number) {
    const deletedEntries = await this.database.deleteUserById(id);

    if (deletedEntries === 0) {
      throw new Error404(`User ID = ${id} doesn't exist`);
    }
  }
}


export class GetUserByIdUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  async execute(id: number) : Promise<User | null> {
    const user = await this.database.getUserById(id);
    if (!user)
      throw new Error404(`User ID = ${id} doesn't exist`);
    return user
  }
}


export class GetAllUsersUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  execute() : Promise<User[]> {
    return this.database.getAllUsers();
  }
}