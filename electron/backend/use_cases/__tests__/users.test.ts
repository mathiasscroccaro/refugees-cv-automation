import {
  CreateUserUseCase,
  GetUserByTokenUseCase,
  GetUserTokenByEmailAndPasswordUseCase,
  UpdateUserByIdUseCase,
} from "use_cases/users";

import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

import { User } from "models/user";
import { GetDatabaseUseCase } from "use_cases/databases";
import { Error400 } from "models/error";

describe("Given all the User's related UseCase", () => {
  it("Should create a new user", async () => {
    const db = new GetDatabaseUseCase().execute();
    const date = new Date().toISOString();

    const mockedDB = jest
      .spyOn(Object.getPrototypeOf(db), "createUser")
      .mockImplementation((): Promise<User> => {
        const user: User = {
          id: 1,
          email: "test@test.com",
          password: "testPassword",
          isAdmin: true,
          createdAt: date,
          updatedAt: date,
        };
        return Promise.resolve(user);
      });

    const user = await new CreateUserUseCase(db).execute({
      email: "test@test.com",
      password: "testPassword",
      isAdmin: true,
    } as User);

    expect(mockedDB).toBeCalledTimes(1);
    expect(user.id).toBe(1);
    expect(user.createdAt).toBe(date);
  });

  it("Should get an user-related token", async () => {
    const db = new GetDatabaseUseCase().execute();
    const mockedSQLiteDB = jest
      .spyOn(Object.getPrototypeOf(db), "getUserByEmail")
      .mockImplementation((): Promise<User> => {
        const user: User = {
          id: 1,
          email: "test@test.com",
          password: bcrypt.hashSync("test", 10),
        };
        return Promise.resolve(user);
      });

    const token = await new GetUserTokenByEmailAndPasswordUseCase(db).execute(
      "test@test.com",
      "test"
    );
    expect(mockedSQLiteDB).toBeCalledTimes(1);
    expect(token).toBeTruthy();
  });

  it("Should return user after supplying the token", async () => {
    const db = new GetDatabaseUseCase().execute();
    const mockedUser: User = {
      id: 1,
      email: "test@test.com",
      password: bcrypt.hashSync("test", 10),
    };

    const mockedSQLiteDB = jest
      .spyOn(Object.getPrototypeOf(db), "getUserById")
      .mockImplementation((): Promise<User> => {
        return Promise.resolve(mockedUser);
      });

    const token = jwt.sign(
      { id: mockedUser.id },
      process.env.TOKEN_SECRET_KEY || "secret"
    );
    const user = await new GetUserByTokenUseCase(db).execute(token);

    expect(mockedSQLiteDB).toBeCalledTimes(1);
    expect(user?.id).toBe(mockedUser.id);
  });

  it("Should update the user by passing the id", async () => {
    const db = new GetDatabaseUseCase().execute();
    const mockedId: number = 1;

    const mockedUser: User = {
      id: mockedId,
      email: "test@test.com",
      password: bcrypt.hashSync("test", 10),
    };

    const changes: Partial<User> = {
      email: "newEmail@test.com",
      isAdmin: true,
    };

    const mockedSQLiteDB = jest
      .spyOn(Object.getPrototypeOf(db), "modifyUserById")
      .mockImplementation((): Promise<User> => {
        return Promise.resolve({ ...mockedUser, ...changes });
      });

    const updatedUser = await new UpdateUserByIdUseCase(db).execute(mockedId, {
      ...changes,
    });

    expect(updatedUser).toEqual({ ...mockedUser, ...changes } as User);
  });

  it("Should raise an error if try to create a duplicated CPF", async () => {
    const db = new GetDatabaseUseCase().execute();
    await db.sync({ force: true });

    const user: User = {
      email: "Testing refugee",
      password: "xxx",
    };

    const mockedDBFirst = jest
      .spyOn(Object.getPrototypeOf(db), "createUser")
      .mockImplementation((): Promise<User> => {
        return Promise.resolve(user);
      });
    await new CreateUserUseCase(db).execute(user);

    const mockedDBSecond = jest
      .spyOn(Object.getPrototypeOf(db), "createUser")
      .mockImplementation((): Promise<null> => {
        return Promise.resolve(null);
      });

    await expect(async () => {
      await new CreateUserUseCase(db).execute(user);
    }).rejects.toThrow(Error400);

    expect(mockedDBFirst).toHaveBeenCalled();
    expect(mockedDBSecond).toHaveBeenCalled();
  });
});
