import { Error400, Error404 } from "models/error";
import { Gender, Refugee } from "models/refugee";
import { GetDatabaseUseCase } from "use_cases/databases";
import {
  CreateRefugeeUseCase,
  UpdateRefugeeByCPFUseCase,
} from "use_cases/refugees";

describe("Given a CreateRefugeeUseCase", () => {
  it("Should raise an error if try to create a duplicated CPF", async () => {
    const db = new GetDatabaseUseCase().execute();
    await db.sync({ force: true });

    const refugee: Refugee = {
      name: "Testing refugee",
      cpf: "000.000.000-00",
      gender: Gender.M,
      dateOfBirth: new Date(),
    };

    const mockedDBFirst = jest
      .spyOn(Object.getPrototypeOf(db), "createRefugee")
      .mockImplementation((): Promise<Refugee> => {
        return Promise.resolve(refugee);
      });
    await new CreateRefugeeUseCase(db).execute(refugee);

    const mockedDBSecond = jest
      .spyOn(Object.getPrototypeOf(db), "createRefugee")
      .mockImplementation((): Promise<null> => {
        return Promise.resolve(null);
      });

    await expect(async () => {
      await new CreateRefugeeUseCase(db).execute(refugee);
    }).rejects.toThrow(Error400);

    expect(mockedDBFirst).toHaveBeenCalled();
    expect(mockedDBSecond).toHaveBeenCalled();
  });
});

describe("Given a UpdateRefugeeUseCase", () => {
  it("Should raise an error if try to update a non existance CPF", async () => {
    const db = new GetDatabaseUseCase().execute();
    await db.sync({ force: true });

    const refugee: Refugee = {
      name: "Testing refugee",
      cpf: "000.000.000-00",
      gender: Gender.M,
      dateOfBirth: new Date(),
    };

    const mockedDBCreate = jest
      .spyOn(Object.getPrototypeOf(db), "createRefugee")
      .mockImplementation((): Promise<Refugee> => {
        return Promise.resolve(refugee);
      });

    const mockedDBUpdate = jest
      .spyOn(Object.getPrototypeOf(db), "updateRefugeeByCPF")
      .mockImplementation((): Promise<null> => {
        return Promise.resolve(null);
      });

    await db.createRefugee(refugee);
    await expect(async () => {
      await new UpdateRefugeeByCPFUseCase(db).execute(
        "111.111.111-11",
        refugee
      );
    }).rejects.toThrow(Error404);

    expect(mockedDBCreate).toHaveBeenCalled();
    expect(mockedDBUpdate).toHaveBeenCalled();
  });
});
