import { Error400, Error404 } from "models/error";
import { Refugee } from "models/refugee";
import { IDataBase } from "database/interfaces";

export class CreateRefugeeUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  async execute(refugee: Refugee): Promise<Refugee> {
    const refugeeToBeCreated = await this.database.createRefugee(refugee);
    if (!refugeeToBeCreated)
      throw new Error400(
        `Refugee with CPF = ${refugee.cpf} cannot be duplicated`
      );
    return refugeeToBeCreated;
  }
}

export class UpdateRefugeeByCPFUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  async execute(cpf: string, refugee: Partial<Refugee>): Promise<Refugee> {
    const refugeeToBeUpdated = await this.database.updateRefugeeByCPF(
      cpf,
      refugee
    );
    if (!refugeeToBeUpdated)
      throw new Error404(`Refugee with CPF = ${cpf} doesn't exist`);
    return refugeeToBeUpdated;
  }
}

export class GetAllRefugeesUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  async execute(): Promise<Refugee[]> {
    return await this.database.getAllRefugees();
  }
}

export class GetRefugeeByCPFUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  async execute(cpf: string): Promise<Refugee> {
    const refugee = await this.database.getRefugeeByCPF(cpf);
    if (!refugee) throw new Error404(`Refugee CPF = ${cpf} doesn't exist`);
    return refugee;
  }
}

export class DeleteRefugeeByCPFUseCase {
  database: IDataBase;

  constructor(database: IDataBase) {
    this.database = database;
  }

  async execute(cpf: string) {
    const num = await this.database.deleteRefugeeByCPF(cpf);
    if (num === 0) throw new Error404(`Refugee CPF = ${cpf} doesn't exist`);
  }
}
