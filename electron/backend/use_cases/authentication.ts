import { Error401, Error500 } from "models/error";
import { User } from "models/user";
import { GetDatabaseUseCase } from "./databases";
import { GetUserByTokenUseCase } from "./users";

export class GetAuthenticatedUserUseCase {
  async execute(token: string | undefined): Promise<User> {
    if (!token || token === "")
      throw new Error401("'x-access-token' not found in the headers");

    let user: User | null;

    try {
      user = await new GetUserByTokenUseCase(
        new GetDatabaseUseCase().execute()
      ).execute(token);
    } catch (error) {
      throw new Error500("Some problem happend while connecting to the DB.");
    }

    if (!user) throw new Error401("Token not valid");

    return user;
  }
}
