export type User = {
  id?: number;
  email: string;
  password: string;
  isAdmin?: boolean;
  createdAt?: string;
  updatedAt?: string;
};

export type Token = string;
