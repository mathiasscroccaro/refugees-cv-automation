type ErrorResponse = {
  auth?: boolean;
  message: string;
};

export abstract class ErrorBase extends Error {
  abstract status_code: number;
  constructor(message: string) {
    super(message);
  }
  abstract getResponse(): ErrorResponse;
}

export class Error400 extends ErrorBase {
  status_code: number;
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, Error400.prototype);
    this.status_code = 400;
  }

  getResponse(): ErrorResponse {
    return {
      message: this.message,
    };
  }
}

export class Error401 extends ErrorBase {
  status_code: number;
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, Error401.prototype);
    this.status_code = 401;
  }

  getResponse(): ErrorResponse {
    return {
      auth: false,
      message: this.message,
    };
  }
}

export class Error403 extends ErrorBase {
  status_code: number;
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, Error403.prototype);
    this.status_code = 403;
  }

  getResponse(): ErrorResponse {
    return {
      auth: true,
      message: this.message,
    };
  }
}

export class Error404 extends ErrorBase {
  status_code: number;
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, Error404.prototype);
    this.status_code = 404;
  }

  getResponse(): ErrorResponse {
    return {
      message: this.message,
      auth: true,
    };
  }
}

export class Error500 extends ErrorBase {
  status_code: number;
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, Error500.prototype);
    this.status_code = 500;
  }

  getResponse(): ErrorResponse {
    return {
      message: this.message,
    };
  }
}
