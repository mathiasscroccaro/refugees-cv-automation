export type OwnBusiness = {
  id?: number;
  refugeeId?: number;
  companyName: string;
  responsible: string;
  startAt?: Date;
  endAt?: Date;
  familyBusiness: boolean;
  employees: number;
  socialMedia?: string;
  invoicing: string;
  description: string;
  localization: string;
  yearsOfExperience?: number;
  currentStatus?: string;
  createdAt?: Date;
  updatedAt?: Date;
};
