import { Course } from "./course";
import { Disabilities } from "./disabilities";
import { Education } from "./education";
import { OwnBusiness } from "./own_business";
import { ProfessionalExperience } from "./professional_experience";
import { Role } from "./role";
import { ShelteringProcess } from "./shelter";

export enum Gender {
  M = "Male",
  F = "Female",
}

export type Refugee = {
  id?: number;
  name: string;
  dateOfBirth: Date;
  cpf: string;
  gender: Gender;

  address?: string;
  email?: string;
  phone?: string;

  disabilities?: Disabilities;

  shelteringProcess?: ShelteringProcess;

  haveCTPS?: boolean;
  mainRole?: Role;
  professionalObjective?: string;

  professionalExperiences?: ProfessionalExperience[];
  courses?: Course[];
  educations?: Education[];
  ownBusiness?: OwnBusiness[];

  createdAt?: Date;
  updatedAt?: Date;
};
