import { Error400, Error401 } from "models/error";

describe("Test the error class", () => {
  it("Should get correct message", () => {
    const error = new Error400("My custom message");
    expect(error.getResponse()).toEqual({ message: "My custom message" });
  });

  it("Should return the auth status and the custom message", () => {
    const error = new Error401("My custom message");
    expect(error.getResponse()).toEqual({
      auth: false,
      message: "My custom message",
    });
  });
});
