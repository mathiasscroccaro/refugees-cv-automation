import { Role } from "./role";

export type ProfessionalExperience = {
  id?: number;
  refugeeId?: number;
  companyName: string;
  startAt: Date;
  endAt: Date;
  localization: string;
  roleDescription: string;
  role: Role;
  createdAt?: Date;
  updatedAt?: Date;
};
