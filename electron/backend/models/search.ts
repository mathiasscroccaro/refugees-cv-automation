import { Role } from "./role";

export type RefugeeSearch = {
    name: string;
    cpf: string;
    mainRole?: Role;
}