export enum ShelterCluster {
  R1 = "Rondon 1",
  R2 = "Rondon 2",
  R5 = "Rondon 5",
  AwT = "Waraotuma Tuaranoko",
  Bv8 = "Brasil/Venezuela 8",
}

export type ShelteringProcess = {
  id?: number;
  shelter: string;
  shelterCluster: ShelterCluster;
  internalizationProcess: boolean;
  internalizationDetails?: string;
  createdAt?: Date;
  updatedAt?: Date;
};
