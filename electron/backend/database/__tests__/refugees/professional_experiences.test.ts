import { Gender, Refugee } from "models/refugee";
import { IDataBase } from "database/interfaces";
import { MemoryDB } from "database/memory_db";
import { ProfessionalExperienceDB } from "database/entities";
import { ProfessionalExperience } from "models/professional_experience";
import { Role } from "models/role";

function arrayHasObject(
  array: ProfessionalExperienceDB[] | ProfessionalExperience[],
  object: ProfessionalExperience | ProfessionalExperienceDB
) {
  expect(array).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        companyName: object.companyName,
      }),
    ])
  );
  expect(array).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        roleDescription: object.roleDescription,
      }),
    ])
  );
}

describe("Given a empty DB", () => {
  let db: IDataBase;
  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });
  });
  it("Should create a RefugeeDB entry with ProfessionalExperienceDB relationship", async () => {
    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",

      professionalExperiences: [
        {
          companyName: "Testing company",
          localization: "Testing location",
          role: {
            roleName: "software developer",
          },
          roleDescription: "Create web pages for e-commerces",
          startAt: new Date(),
          endAt: new Date(),
        },
      ],
    };

    const roleToBeCreated: Role = {
      roleName: "software developer",
    };
    await db.findUpdateOrCreateRole(roleToBeCreated);

    const refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;

    if (
      refugee.professionalExperiences &&
      refugeeToBeCreated.professionalExperiences
    ) {
      arrayHasObject(
        refugee.professionalExperiences,
        refugeeToBeCreated.professionalExperiences[0]
      );
    }
  });
});

describe("Given a refugee in the DB without a ProfessionalExperienceDB relationship", () => {
  let db: IDataBase;
  let refugee: Refugee;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });

    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",
    };

    const roleToBeCreated: Role = {
      roleName: "software developer",
    };
    await db.findUpdateOrCreateRole(roleToBeCreated);

    refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;
  });

  it("Should append a ProfessionalExperienceDB relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      professionalExperiences: [
        {
          companyName: "Testing company",
          localization: "Testing location",
          role: {
            roleName: "software developer",
          },
          roleDescription: "Create web pages for e-commerces",
          startAt: new Date(),
          endAt: new Date(),
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (
      refugeeInfoToBeUpdated.professionalExperiences &&
      returnedRefugee?.professionalExperiences
    ) {
      arrayHasObject(
        returnedRefugee.professionalExperiences,
        refugeeInfoToBeUpdated.professionalExperiences[0]
      );
    }
  });
  it("Should append multiple ProfessionalExperienceDB relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      professionalExperiences: [
        {
          companyName: "Testing company 1",
          localization: "Testing location",
          role: {
            roleName: "software developer",
          },
          roleDescription: "Create web pages for e-commerces 1",
          startAt: new Date(),
          endAt: new Date(),
        },
        {
          companyName: "Testing company 2",
          localization: "Testing location",
          role: {
            roleName: "software developer",
          },
          roleDescription: "Create web pages for e-commerces 2",
          startAt: new Date(),
          endAt: new Date(),
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (
      refugeeInfoToBeUpdated.professionalExperiences &&
      returnedRefugee?.professionalExperiences
    ) {
      arrayHasObject(
        returnedRefugee.professionalExperiences,
        refugeeInfoToBeUpdated.professionalExperiences[0]
      );
      arrayHasObject(
        returnedRefugee.professionalExperiences,
        refugeeInfoToBeUpdated.professionalExperiences[1]
      );
    }
  });
});

describe("Given a refugee in the DB with a ProfessionalExperienceDB relationship", () => {
  let db: IDataBase;
  let refugee: Refugee;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });

    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",

      professionalExperiences: [
        {
          companyName: "Testing company",
          localization: "Testing location",
          role: {
            roleName: "software developer",
          },
          roleDescription: "Create web pages for e-commerces",
          startAt: new Date(),
          endAt: new Date(),
        },
      ],
    };

    const roleToBeCreated: Role = {
      roleName: "software developer",
    };
    await db.findUpdateOrCreateRole(roleToBeCreated);

    refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;
  });

  it("Should update the previouly saved ProfessionalExperienceDB data", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      professionalExperiences: [
        {
          id: 1,
          companyName: "Testing company - modified",
          localization: "Testing location",
          role: {
            roleName: "software developer",
          },
          roleDescription: "Create web pages for e-commerces - modified",
          startAt: new Date(),
          endAt: new Date(),
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (
      refugeeInfoToBeUpdated.professionalExperiences &&
      returnedRefugee?.professionalExperiences
    ) {
      arrayHasObject(
        returnedRefugee.professionalExperiences,
        refugeeInfoToBeUpdated.professionalExperiences[0]
      );
      expect((await ProfessionalExperienceDB.findAll()).length).toBe(1);
      expect(returnedRefugee.professionalExperiences.length).toBe(1);
    }
  });
  it("Should append a ProfessionalExperienceDB entry", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      professionalExperiences: [
        {
          id: 1,
          companyName: "Testing company - modified",
          localization: "Testing location",
          role: {
            roleName: "software developer",
          },
          roleDescription: "Create web pages for e-commerces - modified",
          startAt: new Date(),
          endAt: new Date(),
        },
        {
          companyName: "New Testing company",
          localization: "Testing location",
          role: {
            roleName: "software developer",
          },
          roleDescription: "Maintain web pages for e-commerces",
          startAt: new Date(),
          endAt: new Date(),
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (
      refugeeInfoToBeUpdated.professionalExperiences &&
      returnedRefugee?.professionalExperiences
    ) {
      arrayHasObject(
        returnedRefugee.professionalExperiences,
        refugeeInfoToBeUpdated.professionalExperiences[0]
      );
      expect((await ProfessionalExperienceDB.findAll()).length).toBe(2);
      expect(returnedRefugee.professionalExperiences.length).toBe(2);
    }
  });
  it("Should remove the relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      professionalExperiences: [],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (
      refugeeInfoToBeUpdated.professionalExperiences &&
      returnedRefugee?.professionalExperiences
    ) {
      expect((await ProfessionalExperienceDB.findAll()).length).toBe(1);
      expect(returnedRefugee.professionalExperiences.length).toBe(0);
    }
  });
});
