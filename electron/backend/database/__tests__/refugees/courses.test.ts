import { Gender, Refugee } from "models/refugee";
import { IDataBase } from "database/interfaces";
import { MemoryDB } from "database/memory_db";
import { CourseDB } from "database/entities";
import { Course } from "models/course";

function arrayHasObject(
  array: CourseDB[] | Course[],
  object: Course | CourseDB
) {
  expect(array).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        institution: object.institution,
      }),
    ])
  );
  expect(array).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        year: object.year,
      }),
    ])
  );
}

describe("Given a empty DB", () => {
  let db: IDataBase;
  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });
  });
  it("Should create a RefugeeDB entry with CourseDB relationship", async () => {
    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",

      courses: [
        {
          institution: 'Testing Institution',
          name: 'Test Course',
          supportedBy: 'ONU',
          year: 2020
        },
      ],
    };

    const refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;

    if (refugee.courses && refugeeToBeCreated.courses) {
      arrayHasObject(refugee.courses, refugeeToBeCreated.courses[0]);
    }
  });
});

describe("Given a refugee in the DB without a CourseDB relationship", () => {
  let db: IDataBase;
  let refugee: Refugee;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });

    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",
    };

    refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;
  });

  it("Should append a CourseDB relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      courses: [
        {
          institution: 'Testing Institution',
          name: 'Test Course',
          supportedBy: 'ONU',
          year: 2020
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.courses && returnedRefugee?.courses) {
      arrayHasObject(
        returnedRefugee.courses,
        refugeeInfoToBeUpdated.courses[0]
      );
    }
  });
  it("Should append multiple CourseDB relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      courses: [
        {
          institution: 'Testing Institution 1',
          name: 'Test Course',
          supportedBy: 'ONU',
          year: 2020
        },
        {
          institution: 'Testing Institution 2',
          name: 'Test Course',
          supportedBy: 'ONU',
          year: 2019
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.courses && returnedRefugee?.courses) {
      arrayHasObject(
        returnedRefugee.courses,
        refugeeInfoToBeUpdated.courses[0]
      );
      arrayHasObject(
        returnedRefugee.courses,
        refugeeInfoToBeUpdated.courses[1]
      );
    }
  });
});

describe("Given a refugee in the DB with a CourseDB relationship", () => {
  let db: IDataBase;
  let refugee: Refugee;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });

    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",

      courses: [{
        institution: 'Testing Institution',
        name: 'Test Course',
        supportedBy: 'ONU',
        year: 2020
      }],
    };

    refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;
  });

  it("Should update the previouly saved CourseDB data", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      courses: [
        {
          id: 1,
          institution: 'Testing Institution - modified',
          name: 'Test Course',
          supportedBy: 'ONU',
          year: 2010
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.courses && returnedRefugee?.courses) {
      arrayHasObject(
        returnedRefugee.courses,
        refugeeInfoToBeUpdated.courses[0]
      );
      expect((await CourseDB.findAll()).length).toBe(1);
      expect(returnedRefugee.courses.length).toBe(1);
    }
  });
  it("Should append a CourseDB entry", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      courses: [
        {
          id: 1,
          institution: 'Testing Institution - modified',
          name: 'Test Course',
          supportedBy: 'ONU',
          year: 2015
        },
        {
          institution: 'New Testing Institution',
          name: 'Test Course',
          supportedBy: 'ONU',
          year: 2018
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.courses && returnedRefugee?.courses) {
      arrayHasObject(
        returnedRefugee.courses,
        refugeeInfoToBeUpdated.courses[0]
      );
      expect((await CourseDB.findAll()).length).toBe(2);
      expect(returnedRefugee.courses.length).toBe(2);
    }
  });
  it("Should remove the relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
        cpf: refugee.cpf,
  
        courses: [],
      };
  
      const returnedRefugee = await db.updateRefugeeByCPF(
        refugee.cpf,
        refugeeInfoToBeUpdated
      );
  
      if (refugeeInfoToBeUpdated.courses && returnedRefugee?.courses) {
        expect((await CourseDB.findAll()).length).toBe(1);
        expect(returnedRefugee.courses.length).toBe(0);
      }
  });
});
