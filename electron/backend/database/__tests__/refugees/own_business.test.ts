import { Gender, Refugee } from "models/refugee";
import { IDataBase } from "database/interfaces";
import { MemoryDB } from "database/memory_db";
import { OwnBusinessDB } from "database/entities";
import { OwnBusiness } from "models/own_business";

function arrayHasObject(
  array: OwnBusinessDB[] | OwnBusiness[],
  object: OwnBusiness | OwnBusinessDB
) {
  expect(array).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        description: object.description,
      }),
    ])
  );
  expect(array).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        companyName: object.companyName,
      }),
    ])
  );
}

describe("Given a empty DB", () => {
  let db: IDataBase;
  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });
  });
  it("Should create a RefugeeDB entry with OwnBusinessDB relationship", async () => {
    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",

      ownBusiness: [
        {
          companyName: "Test own company",
          description: "Responsible to do a lot of tests",
          employees: 15,
          familyBusiness: false,
          invoicing: "US$ 50k / year",
          localization: "Caracas - VEN",
          responsible: "myself",
        },
      ],
    };

    const refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;

    if (refugee.ownBusiness && refugeeToBeCreated.ownBusiness) {
      arrayHasObject(refugee.ownBusiness, refugeeToBeCreated.ownBusiness[0]);
    }
  });
});

describe("Given a refugee in the DB without a OwnBusinessDB relationship", () => {
  let db: IDataBase;
  let refugee: Refugee;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });

    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",
    };

    refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;
  });

  it("Should append a OwnBusinessDB relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      ownBusiness: [
        {
          companyName: "Test own company",
          description: "Responsible to do a lot of tests modified",
          employees: 15,
          familyBusiness: false,
          invoicing: "US$ 50k / year",
          localization: "Caracas - VEN",
          responsible: "myself",
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.ownBusiness && returnedRefugee?.ownBusiness) {
      arrayHasObject(
        returnedRefugee.ownBusiness,
        refugeeInfoToBeUpdated.ownBusiness[0]
      );
    }
  });
  it("Should append multiple OwnBusinessDB relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      ownBusiness: [
        {
          companyName: "Test own company 1",
          description: "Responsible to do a lot of tests 1",
          employees: 15,
          familyBusiness: false,
          invoicing: "US$ 50k / year",
          localization: "Caracas - VEN",
          responsible: "myself",
        },
        {
          companyName: "Test own company 2",
          description: "Responsible to do a lot of tests 2",
          employees: 15,
          familyBusiness: false,
          invoicing: "US$ 50k / year",
          localization: "Caracas - VEN",
          responsible: "myself",
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.ownBusiness && returnedRefugee?.ownBusiness) {
      arrayHasObject(
        returnedRefugee.ownBusiness,
        refugeeInfoToBeUpdated.ownBusiness[0]
      );
      arrayHasObject(
        returnedRefugee.ownBusiness,
        refugeeInfoToBeUpdated.ownBusiness[1]
      );
    }
  });
});

describe("Given a refugee in the DB with a OwnBusinessDB relationship", () => {
  let db: IDataBase;
  let refugee: Refugee;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });

    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",

      ownBusiness: [
        {
          companyName: "Test own company",
          description: "Responsible to do a lot of tests",
          employees: 15,
          familyBusiness: false,
          invoicing: "US$ 50k / year",
          localization: "Caracas - VEN",
          responsible: "myself",
        },
      ],
    };

    refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;
  });

  it("Should update the previouly saved OwnBusinessDB data", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      ownBusiness: [
        {
          id: 1,
          companyName: "Test own company - modified",
          description: "Responsible to do a lot of tests - modified",
          employees: 15,
          familyBusiness: false,
          invoicing: "US$ 50k / year",
          localization: "Caracas - VEN",
          responsible: "myself",
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.ownBusiness && returnedRefugee?.ownBusiness) {
      arrayHasObject(
        returnedRefugee.ownBusiness,
        refugeeInfoToBeUpdated.ownBusiness[0]
      );
      expect((await OwnBusinessDB.findAll()).length).toBe(1);
      expect(returnedRefugee.ownBusiness.length).toBe(1);
    }
  });
  it("Should append a OwnBusinessDB entry", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      ownBusiness: [
        {
          id: 1,
          companyName: "Test own company",
          description: "Responsible to do a lot of tests",
          employees: 15,
          familyBusiness: false,
          invoicing: "US$ 50k / year",
          localization: "Caracas - VEN",
          responsible: "myself",
        },
        {
          companyName: "Test own company - new one",
          description: "Responsible to do a lot of tests - new one",
          employees: 15,
          familyBusiness: false,
          invoicing: "US$ 50k / year",
          localization: "Caracas - VEN",
          responsible: "myself",
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.ownBusiness && returnedRefugee?.ownBusiness) {
      arrayHasObject(
        returnedRefugee.ownBusiness,
        refugeeInfoToBeUpdated.ownBusiness[0]
      );
      expect((await OwnBusinessDB.findAll()).length).toBe(2);
      expect(returnedRefugee.ownBusiness.length).toBe(2);
    }
  });
  it("Should remove the relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
        cpf: refugee.cpf,
  
        ownBusiness: [],
      };
  
      const returnedRefugee = await db.updateRefugeeByCPF(
        refugee.cpf,
        refugeeInfoToBeUpdated
      );
  
      if (refugeeInfoToBeUpdated.ownBusiness && returnedRefugee?.ownBusiness) {
        expect((await OwnBusinessDB.findAll()).length).toBe(1);
        expect(returnedRefugee.ownBusiness.length).toBe(0);
      }
  });
});
