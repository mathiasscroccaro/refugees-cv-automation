import { Gender, Refugee } from "models/refugee";
import { IDataBase } from "database/interfaces";
import { MemoryDB } from "database/memory_db";
import { EducationDB } from "database/entities";
import { Education, EducationLevel } from "models/education";

function arrayHasObject(
  array: EducationDB[] | Education[],
  object: Education | EducationDB
) {
  expect(array).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        institution: object.institution,
      }),
    ])
  );
  expect(array).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        year: object.year,
      }),
    ])
  );
}

describe("Given a empty DB", () => {
  let db: IDataBase;
  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });
  });
  it("Should create a RefugeeDB entry with EducationDB relationship", async () => {
    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",

      educations: [
        {
          institution: 'Testing University',
          level: EducationLevel.UNDERGRAD,
          year: 2020,
          name: 'Bachelor in Testing'
        },
      ],
    };

    const refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;

    if (refugee.educations && refugeeToBeCreated.educations) {
      arrayHasObject(refugee.educations, refugeeToBeCreated.educations[0]);
    }
  });
});

describe("Given a refugee in the DB without a EducationDB relationship", () => {
  let db: IDataBase;
  let refugee: Refugee;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });

    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",
    };

    refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;
  });

  it("Should append a EducationDB relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      educations: [
        {
          institution: 'Testing University',
          level: EducationLevel.UNDERGRAD,
          year: 2020,
          name: 'Bachelor in Testing'
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.educations && returnedRefugee?.educations) {
      arrayHasObject(
        returnedRefugee.educations,
        refugeeInfoToBeUpdated.educations[0]
      );
    }
  });
  it("Should append multiple EducationDB relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      educations: [
        {
          institution: 'Testing University 1',
          level: EducationLevel.UNDERGRAD,
          year: 2021,
          name: 'Bachelor in Testing'
        },
        {
          institution: 'Testing University 2',
          level: EducationLevel.UNDERGRAD,
          year: 2020,
          name: 'Bachelor in Testing'
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.educations && returnedRefugee?.educations) {
      arrayHasObject(
        returnedRefugee.educations,
        refugeeInfoToBeUpdated.educations[0]
      );
      arrayHasObject(
        returnedRefugee.educations,
        refugeeInfoToBeUpdated.educations[1]
      );
    }
  });
});

describe("Given a refugee in the DB with a EducationDB relationship", () => {
  let db: IDataBase;
  let refugee: Refugee;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });

    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",

      educations: [{
        institution: 'Testing University',
        level: EducationLevel.UNDERGRAD,
        year: 2020,
        name: 'Bachelor in Testing'
      }],
    };

    refugee = await db.createRefugee(refugeeToBeCreated) as Refugee;
  });

  it("Should update the previouly saved EducationDB data", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      educations: [
        {
          id: 1,
          institution: 'Testing University modified',
          level: EducationLevel.UNDERGRAD,
          year: 2000,
          name: 'Bachelor in Testing'
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.educations && returnedRefugee?.educations) {
      arrayHasObject(
        returnedRefugee.educations,
        refugeeInfoToBeUpdated.educations[0]
      );
      expect((await EducationDB.findAll()).length).toBe(1);
      expect(returnedRefugee.educations.length).toBe(1);
    }
  });
  it("Should append a EducationDB entry", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      educations: [
        {
          id: 1,
          institution: 'Testing University modified',
          level: EducationLevel.UNDERGRAD,
          year: 2000,
          name: 'Bachelor in Testing'
        },
        {
          institution: 'New Testing University',
          level: EducationLevel.UNDERGRAD,
          year: 2019,
          name: 'Bachelor in Testing'
        },
      ],
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );

    if (refugeeInfoToBeUpdated.educations && returnedRefugee?.educations) {
      arrayHasObject(
        returnedRefugee.educations,
        refugeeInfoToBeUpdated.educations[0]
      );
      expect((await EducationDB.findAll()).length).toBe(2);
      expect(returnedRefugee.educations.length).toBe(2);
    }
  });
  it("Should remove the relationship", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
        cpf: refugee.cpf,
  
        educations: [],
      };
  
      const returnedRefugee = await db.updateRefugeeByCPF(
        refugee.cpf,
        refugeeInfoToBeUpdated
      );
  
      if (refugeeInfoToBeUpdated.educations && returnedRefugee?.educations) {
        expect((await EducationDB.findAll()).length).toBe(1);
        expect(returnedRefugee.educations.length).toBe(0);
      }
  });
});
