import { ShelterCluster } from "models/shelter";
import { Gender, Refugee } from "models/refugee";
import { Role } from "models/role";
import { IDataBase } from "database/interfaces";
import { MemoryDB } from "database/memory_db";
import {
  DisabilitiesDB,
  RefugeeDB,
  ShelteringProcessDB,
  RoleDB,
} from "database/entities";

describe("Given a empty DB", () => {
  let db: IDataBase;
  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });
  });
  it("Should create a basic RefugeeDB entry", async () => {
    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",
    };

    const refugee = await db.createRefugee(refugeeToBeCreated);

    expect(refugee).toEqual(expect.objectContaining(refugeeToBeCreated));
  });
});

describe("Given a refugee in the DB", () => {
  let db: IDataBase;
  let refugee: Refugee;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });

    const refugeeToBeCreated: Refugee = {
      name: "Test refugee name",
      cpf: "00011122233",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",

      disabilities: {
        disabilitiesMedicalReport: "Test report",
        whichDisabilities: "blind",
      },

      mainRole: {
        roleName: "software developer",
      },

      shelteringProcess: {
        internalizationProcess: true,
        shelter: "UX1",
        shelterCluster: ShelterCluster.R1,
        internalizationDetails: "He is going to somewhere else",
      },
    };

    const roleToBeCreated: Role = {
      roleName: "software developer",
    };

    await db.findUpdateOrCreateRole(roleToBeCreated);
    refugee = (await db.createRefugee(refugeeToBeCreated)) as Refugee;
  });

  it("Should get the refugee by CPF number", async () => {
    const returnedRefugee = await db.getRefugeeByCPF(refugee.cpf);

    expect(returnedRefugee).toEqual(
      expect.objectContaining({ cpf: refugee.cpf, name: refugee.name })
    );
  });

  it("Should return null if the CPF doesn't exist", async () => {
    const returnedRefugee = await db.getRefugeeByCPF('xxx');

    expect(returnedRefugee).toBeNull();
  });

  it("Should update the basic information", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      name: "New refugee name",
      dateOfBirth: new Date(),
      gender: Gender.F,
      address: "Test new street, 200",
      email: "newTest@test.com",
      phone: "(02) 0202-0202",
      haveCTPS: true,
      professionalObjective: "To do more tests",
    };

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );
    const refugeeFromDB = await RefugeeDB.findOne({
      where: { cpf: refugee.cpf },
    });

    expect(returnedRefugee).toEqual(
      expect.objectContaining(refugeeInfoToBeUpdated)
    );
    expect(refugeeFromDB).toEqual(
      expect.objectContaining(refugeeInfoToBeUpdated)
    );
  });

  it("Should update the extended basic information (disabilities, mainRole, shelteringProcess)", async () => {
    const refugeeInfoToBeUpdated: Partial<Refugee> = {
      cpf: refugee.cpf,

      disabilities: {
        disabilitiesMedicalReport: "New Test report",
        whichDisabilities: "physical",
      },
      mainRole: {
        roleName: "software engineer",
      },
      shelteringProcess: {
        internalizationProcess: false,
        shelter: "UX2",
        shelterCluster: ShelterCluster.R2,
        internalizationDetails: "He is going to stay",
      },
    };

    const roleToBeCreated: Role = {
      roleName: "software engineer",
    };
    await db.findUpdateOrCreateRole(roleToBeCreated);

    const returnedRefugee = await db.updateRefugeeByCPF(
      refugee.cpf,
      refugeeInfoToBeUpdated
    );
    const refugeeFromDB = await RefugeeDB.findOne({
      where: { cpf: refugee.cpf },
      include: "mainRole",
    });

    expect(returnedRefugee?.disabilities).toEqual(
      expect.objectContaining(refugeeInfoToBeUpdated.disabilities)
    );
    expect(await refugeeFromDB?.getDisabilities()).toEqual(
      expect.objectContaining(refugeeInfoToBeUpdated.disabilities)
    );

    expect(returnedRefugee?.mainRole).toEqual(
      expect.objectContaining(refugeeInfoToBeUpdated.mainRole)
    );
    expect(await (refugeeFromDB as Refugee).mainRole).toEqual(
      expect.objectContaining(refugeeInfoToBeUpdated.mainRole)
    );

    expect(returnedRefugee?.shelteringProcess).toEqual(
      expect.objectContaining(refugeeInfoToBeUpdated.shelteringProcess)
    );
    expect(await refugeeFromDB?.getShelteringProcess()).toEqual(
      expect.objectContaining(refugeeInfoToBeUpdated.shelteringProcess)
    );

    expect((await DisabilitiesDB.findAll()).length).toBe(1);
    expect((await RoleDB.findAll()).length).toBe(2);
    expect((await ShelteringProcessDB.findAll()).length).toBe(1);
  });

  it("Should return a list of all refugees", async () => {
    const newRefugee: Refugee = {
      name: "Test2",
      cpf: "000111222",
      dateOfBirth: new Date(),
      gender: Gender.M,
      address: "Test street, 200",
      email: "test@test.com",
      phone: "(01) 0101-0101",
      haveCTPS: false,
      professionalObjective: "To do some tests",
    };
    await db.createRefugee(newRefugee);

    const refugeeList = await db.getAllRefugees();

    expect(refugeeList).toEqual(
      expect.arrayContaining([expect.objectContaining({ name: "Test2" })])
    );
    expect(refugeeList).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ name: "Test refugee name" }),
      ])
    );
  });

  it("Should return null if try to create a refugee with the same CPF", async () => {
    const newRefugee: Refugee = {
      ...refugee,
    };
    expect(await db.createRefugee(newRefugee)).toBeNull();
  });

  it('Should return 1 when trying to delete the refugee by CPF', async () => {
    expect(await db.deleteRefugeeByCPF(refugee.cpf)).toBe(1);
  });

  it('Should return 0 when trying to delete non existing CPF refugee', async () => {
    expect(await db.deleteRefugeeByCPF('xxx')).toBe(0);
  });
});
