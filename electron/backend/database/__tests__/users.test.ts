import { SQLiteDB } from 'database/sqlite_db';
import { sequelize } from 'database/index'
import { User } from 'models/user';
import { UserDB } from 'database/entities/users';


describe('When the SQLite database is empty', () => {
    beforeEach(async () => {
        await sequelize.sync({force: true});
    })
    it('Should create a user', async () => {
        const db = new SQLiteDB();
        await db.createUser({
            email: 'test@test.com',
            password: 'testPassword',
        } as User);
        const testUser = await UserDB.findOne({
            where: {
                email: 'test@test.com'
            }
        });
        expect(testUser?.isAdmin).toBe(false);
        expect(testUser?.email).toBe('test@test.com');
    });
    it('Should return null when looking for a invalid email', async () => {
        const testUser = await UserDB.findOne({
            where: {
                email: 'test@test.com'
            }
        });
        expect(testUser).toBe(null);
    })
});

describe('When the SQLite database already has one user', () => {
    beforeEach(async () => {
        await sequelize.sync({force: true});
        
        await new SQLiteDB().createUser({
            email: 'test@test.com',
            password: 'testPassword',
            isAdmin: false
        } as User);
    })

    it('Should update the user email and permission', async () => {
        const updatedUser = await new SQLiteDB().modifyUserById(1, {email: 'newEmail@test.com', isAdmin: true});
        expect(updatedUser?.email).toBe('newEmail@test.com');
        expect(updatedUser?.isAdmin).toBe(true);
    });

    it('Should get the user by the ID', async () => {
        const user = await new SQLiteDB().getUserById(1);
        expect(user?.email).toBe('test@test.com');
        expect(user?.isAdmin).toBe(false);
    });

    it('Should get the user by email', async () => {
        const user = await new SQLiteDB().getUserByEmail('test@test.com');
        expect(user?.email).toBe('test@test.com');
        expect(user?.isAdmin).toBe(false);
    });

    it('Should get null for an invalid email query', async () => {
        const user = await new SQLiteDB().getUserByEmail('test@google.com');
        expect(user).toBeNull();
    });

    it('Should delete the user by ID and return empty list', async () => {
        await new SQLiteDB().deleteUserById(1);
        const users = await new SQLiteDB().getAllUsers();
        expect(users).toEqual([]);
    });

    it('Should return null when updating invalid ID user', async () => {
        const invalidUpdatedUser = await new SQLiteDB().modifyUserById(2, {email: 'newEmail@test.com'});
        expect(invalidUpdatedUser).toBeNull();
    })

    it('Should create a new user and list all of them', async () => {
        await new SQLiteDB().createUser({email: 'test2@test.com', password: 'testPassword'} as User);
        const users = await new SQLiteDB().getAllUsers();
        expect(users).toBeTruthy();
        if (users) {
            expect(users[0].email).toBe('test@test.com');
            expect(users[1].email).toBe('test2@test.com');
        }
    });
})