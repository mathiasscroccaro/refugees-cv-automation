import { IDataBase } from 'database/interfaces';
import { MemoryDB } from 'database/memory_db'
import { Role } from 'models/role';

describe('When the database is empty', () => {
    let db: IDataBase;

    beforeEach(async () => {
        db = new MemoryDB();
        await db.sync({force: true});
    });


    it('Should create a Role', async () => {
        const roleToBeCreated: Role = {
            roleName: 'software developer',
            description: 'A profissional who makes software'
        }

        const role = await db.findUpdateOrCreateRole(roleToBeCreated);

        expect(role.roleName).toEqual(roleToBeCreated.roleName)
        expect(role.description).toEqual(roleToBeCreated.description)
    })
});

describe('When there is previouly a role entry', () => {
    let db: IDataBase;
    let createdRole: Role;

    beforeEach(async () => {
        db = new MemoryDB();
        await db.sync({force: true});

        const roleToBeCreated: Role = {
            roleName: 'software developer',
            description: 'A profissional who makes software'
        }

        createdRole = await db.findUpdateOrCreateRole(roleToBeCreated);
    });

    it('Should delete the Role', async () => {
        const numberOfDeletedRoles =  await db.deleteRoleById(createdRole.id as number);
        expect(numberOfDeletedRoles).toEqual(1);
    });

    it('Should update the Role', async () => {
        const roleToBeUpdated: Role = {
            ...createdRole,
            description: 'A profissional who makes good software'
        };
        const updatedRole = await db.findUpdateOrCreateRole(roleToBeUpdated);
        expect(updatedRole.roleName).toEqual(roleToBeUpdated.roleName);
        expect(updatedRole.description).toEqual(roleToBeUpdated.description);
    })
})