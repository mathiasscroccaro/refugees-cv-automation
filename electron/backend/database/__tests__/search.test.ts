import { Gender, Refugee, Role } from "@/models";
import { IDataBase } from "../interfaces";
import { MemoryDB } from "../memory_db";

describe("When the database is empty", () => {
  let db: IDataBase;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });
  });

  it("Shouldnt find any refugee", async () => {
    const query = "Fake Refugee";

    const refugeeSearch = await db.searchRefugeeByCPF(query);

    expect(refugeeSearch).toEqual([]);
  });
});

describe("When there is previouly a role entry", () => {
  let db: IDataBase;
  let refugee: Refugee;

  beforeEach(async () => {
    db = new MemoryDB();
    await db.sync({ force: true });

    const roleToBeCreated: Role = {
      roleName: "software developer",
      description: "A profissional who makes software",
    };

    const createdRole = await db.findUpdateOrCreateRole(roleToBeCreated);

    const refugeeToBeCreated: Refugee = {
      cpf: "00011122233",
      name: "Test Refugee",
      dateOfBirth: new Date(),
      gender: Gender.M,
      mainRole: {
        roleName: "software developer",
      },
    };

    refugee = (await db.createRefugee(refugeeToBeCreated)) as Refugee;
  });

  it("Should find the refugee by partial cpf", async () => {
    expect(await db.searchRefugeeByCPF("000")).toEqual(
      expect.arrayContaining([expect.objectContaining({ cpf: refugee.cpf })])
    );
    expect(await db.searchRefugeeByCPF("111")).toEqual(
      expect.arrayContaining([expect.objectContaining({ cpf: refugee.cpf })])
    );
    expect(await db.searchRefugeeByCPF("222")).toEqual(
      expect.arrayContaining([expect.objectContaining({ cpf: refugee.cpf })])
    );
    expect(await db.searchRefugeeByCPF("33")).toEqual(
      expect.arrayContaining([expect.objectContaining({ cpf: refugee.cpf })])
    );
    expect(await db.searchRefugeeByCPF("00011122233")).toEqual(
      expect.arrayContaining([expect.objectContaining({ cpf: refugee.cpf })])
    );
  });

  it("Should find the refugee by partial name", async () => {
    expect(await db.searchRefugeeByName("Test")).toEqual(
      expect.arrayContaining([expect.objectContaining({ name: refugee.name })])
    );
    expect(await db.searchRefugeeByName("Te")).toEqual(
      expect.arrayContaining([expect.objectContaining({ name: refugee.name })])
    );
    expect(await db.searchRefugeeByName("Refugee")).toEqual(
      expect.arrayContaining([expect.objectContaining({ name: refugee.name })])
    );
    expect(await db.searchRefugeeByName("Test Refugee")).toEqual(
      expect.arrayContaining([expect.objectContaining({ name: refugee.name })])
    );
  });

  // it('Should update the Role', async () => {
  //     const roleToBeUpdated: Role = {
  //         ...createdRole,
  //         description: 'A profissional who makes good software'
  //     };
  //     const updatedRole = await db.findUpdateOrCreateRole(roleToBeUpdated);
  //     expect(updatedRole.roleName).toEqual(roleToBeUpdated.roleName);
  //     expect(updatedRole.description).toEqual(roleToBeUpdated.description);
  // })
});
