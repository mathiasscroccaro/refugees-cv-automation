import { RefugeeSearch } from "@/models";
import bcrypt from "bcryptjs";
import { Op } from "sequelize";
import { Refugee } from "../models/refugee";
import { Role } from "../models/role";
import { User } from "../models/user";
import { RefugeeDB } from "./entities";
import { RoleDB } from "./entities/role";
import { UserDB } from "./entities/users";
import { IDataBase } from "./interfaces";

export abstract class BaseSQLiteDB extends IDataBase {
  async getUserByEmail(email: string): Promise<User | null> {
    try {
      const query = await UserDB.findOne({ where: { email } });
      if (!query) return Promise.resolve(null);
      else return Promise.resolve(query.toJSON());
    } catch (error) {
      throw error;
    }
  }

  async getUserById(id: number): Promise<User | null> {
    try {
      const query = await UserDB.findOne({ where: { id } });
      if (!query) return Promise.resolve(null);
      else return Promise.resolve(query.toJSON());
    } catch (error) {
      throw error;
    }
  }

  async getAllUsers(): Promise<User[]> {
    try {
      const query = await UserDB.findAll();
      if (!query) return Promise.resolve([]);
      else return Promise.resolve(query.map((value) => value.toJSON()));
    } catch (error) {
      throw error;
    }
  }

  async createUser(user: User): Promise<User> {
    try {
      const createdUser = await UserDB.create({
        ...user,
        password: bcrypt.hashSync(user.password, 10),
      });
      return Promise.resolve(createdUser.toJSON());
    } catch (error) {
      throw error;
    }
  }

  async modifyUserById(id: number, user: Partial<User>): Promise<User | null> {
    try {
      const userToBeModified = await UserDB.findOne({ where: { id } });
      if (!userToBeModified) return Promise.resolve(null);

      userToBeModified.set({
        ...user,
      });
      if (user.password)
        userToBeModified.password = bcrypt.hashSync(user.password, 10);

      await userToBeModified.save();
      return Promise.resolve(userToBeModified.toJSON());
    } catch (error) {
      throw error;
    }
  }

  async deleteUserById(id: number): Promise<number> {
    return UserDB.destroy({
      where: {
        id,
      },
    });
  }

  async getAllRefugees(): Promise<Refugee[]> {
    try {
      const query = await RefugeeDB.scope("formatted").findAll();
      if (!query) return Promise.resolve([]);
      else
        return Promise.resolve(
          query.map((value) => value.toJSON()) as Refugee[]
        );
    } catch (error) {
      throw error;
    }
  }

  async getRefugeeByCPF(cpf: string): Promise<Refugee | null> {
    try {
      const query = await RefugeeDB.findOne({ where: { cpf } });
      if (!query) return Promise.resolve(null);
      else return Promise.resolve(query.toJSON() as Refugee);
    } catch (error) {
      throw error;
    }
  }

  async deleteRefugeeByCPF(cpf: string): Promise<number> {
    try {
      const refugee = await RefugeeDB.findOne({ where: { cpf } });
      if (!refugee) return Promise.resolve(0);
      await refugee.destroy();
      return Promise.resolve(1);
    } catch (error) {
      throw error;
    }
  }

  async createRefugee(refugee: Refugee): Promise<Refugee | null> {
    try {
      const existingRefugee = await RefugeeDB.findOne({
        where: { cpf: refugee.cpf },
      });
      if (existingRefugee) return Promise.resolve(null);

      const createdRefugee = await RefugeeDB.create(refugee);
      if (refugee.disabilities) {
        await createdRefugee.createDisabilities({ ...refugee.disabilities });
      }
      if (refugee.mainRole) {
        const role = await RoleDB.findOne({
          where: {
            roleName: refugee.mainRole.roleName,
          },
        });
        if (role) await createdRefugee.setMainRole(role);
      }
      if (refugee.shelteringProcess) {
        await createdRefugee.createShelteringProcess({
          ...refugee.shelteringProcess,
        });
      }
      if (refugee.courses) {
        refugee.courses.forEach(
          async (course) => await createdRefugee.createCourse({ ...course })
        );
      }
      if (refugee.educations) {
        refugee.educations.forEach(
          async (education) =>
            await createdRefugee.createEducation({ ...education })
        );
      }
      if (refugee.professionalExperiences) {
        for (let professionalExperience of refugee.professionalExperiences) {
          const role = await RoleDB.findOne({
            where: {
              roleName: professionalExperience.role.roleName,
            },
          });
          if (role)
            await createdRefugee.createProfessionalExperience({
              ...professionalExperience,
              roleId: role.id,
            });
        }
      }
      if (refugee.ownBusiness) {
        refugee.ownBusiness.forEach(
          async (ownBusiness) =>
            await createdRefugee.createOwnBusiness({ ...ownBusiness })
        );
      }
      const returnRefugee = await RefugeeDB.scope(["formatted"]).findByPk(
        createdRefugee.id
      );
      return Promise.resolve(returnRefugee?.toJSON() as Refugee);
    } catch (error) {
      throw error;
    }
  }

  async updateDisabilities(
    refugeeToBeUpdated: RefugeeDB,
    newRefugee: Partial<Refugee>
  ) {
    const disabilities = await refugeeToBeUpdated.getDisabilities();
    if (newRefugee.disabilities) {
      if (!disabilities)
        await refugeeToBeUpdated.createDisabilities({
          ...newRefugee.disabilities,
        });
      else {
        disabilities.update({ ...newRefugee.disabilities });
      }
    }
  }

  async updateRole(
    refugeeToBeUpdated: RefugeeDB,
    newRefugee: Partial<Refugee>
  ) {
    if (newRefugee.mainRole) {
      const role = await RoleDB.findOne({
        where: {
          roleName: newRefugee.mainRole.roleName,
        },
      });
      if (role) await refugeeToBeUpdated.setMainRole(role);
    }
  }

  async updateShelteringProcess(
    refugeeToBeUpdated: RefugeeDB,
    newRefugee: Partial<Refugee>
  ) {
    const shelteringProcess = await refugeeToBeUpdated.getShelteringProcess();
    if (newRefugee.shelteringProcess) {
      if (!shelteringProcess)
        await refugeeToBeUpdated.createShelteringProcess({
          ...newRefugee.shelteringProcess,
        });
      else {
        shelteringProcess.update({ ...newRefugee.shelteringProcess });
      }
    }
  }

  async updateCourses(
    refugeeToBeUpdated: RefugeeDB,
    newRefugee: Partial<Refugee>
  ) {
    const courses = await refugeeToBeUpdated.getCourses();
    await refugeeToBeUpdated.setCourses([]);
    if (newRefugee.courses) {
      if (!courses)
        newRefugee.courses.forEach(
          async (course) => await refugeeToBeUpdated.createCourse({ ...course })
        );
      else {
        newRefugee.courses.forEach(async (course) => {
          const courseToBeModified = await courses.find(
            (element) => element.id == course.id
          );
          if (courseToBeModified) {
            courseToBeModified.update({ ...course });
            refugeeToBeUpdated.addCourse(courseToBeModified);
          } else {
            refugeeToBeUpdated.createCourse({ ...course });
          }
        });
      }
    }
  }

  async updateEducations(
    refugeeToBeUpdated: RefugeeDB,
    newRefugee: Partial<Refugee>
  ) {
    const educations = await refugeeToBeUpdated.getEducations();
    await refugeeToBeUpdated.setEducations([]);
    if (newRefugee.educations) {
      if (!educations)
        newRefugee.educations.forEach(
          async (education) =>
            await refugeeToBeUpdated.createEducation({ ...education })
        );
      else {
        newRefugee.educations.forEach(async (education) => {
          const educationToBeModified = await educations.find(
            (element) => element.id == education.id
          );
          if (educationToBeModified) {
            educationToBeModified.update({ ...education });
            refugeeToBeUpdated.addEducation(educationToBeModified);
          } else {
            refugeeToBeUpdated.createEducation({ ...education });
          }
        });
      }
    }
  }

  async updateProfessionalExperiences(
    refugeeToBeUpdated: RefugeeDB,
    newRefugee: Partial<Refugee>
  ) {
    const professionalExperiences =
      await refugeeToBeUpdated.getProfessionalExperiences();
    await refugeeToBeUpdated.setProfessionalExperiences([]);
    if (newRefugee.professionalExperiences) {
      if (!professionalExperiences)
        newRefugee.professionalExperiences.forEach(
          async (professionalExperience) => {
            const role = await RoleDB.findOne({
              where: {
                roleName: professionalExperience.role.roleName,
              },
            });
            if (role)
              await refugeeToBeUpdated.createProfessionalExperience({
                ...professionalExperience,
                roleId: role.id,
              });
          }
        );
      else {
        newRefugee.professionalExperiences.forEach(
          async (professionalExperience) => {
            const professionalExperienceToBeModified =
              await professionalExperiences.find(
                (element) => element.id == professionalExperience.id
              );

            // Find successfully the experience
            if (professionalExperienceToBeModified) {
              await professionalExperienceToBeModified.update({
                ...professionalExperience,
              });
              if (professionalExperience.role) {
                const role = await RoleDB.findOne({
                  where: {
                    roleName: professionalExperience.role.roleName,
                  },
                });
                if (role)
                  await professionalExperienceToBeModified.update({
                    roleId: role.id,
                  });
                await refugeeToBeUpdated.addProfessionalExperience(
                  professionalExperienceToBeModified
                );
              }
            } else {
              const role = await RoleDB.findOne({
                where: {
                  roleName: professionalExperience.role.roleName,
                },
              });
              if (role)
                await refugeeToBeUpdated.createProfessionalExperience({
                  ...professionalExperience,
                  roleId: role.id,
                });
            }
          }
        );
      }
    }
    await refugeeToBeUpdated.save();
  }

  async updateOwnBusiness(
    refugeeToBeUpdated: RefugeeDB,
    newRefugee: Partial<Refugee>
  ) {
    const ownBusinesses = await refugeeToBeUpdated.getOwnBusiness();
    await refugeeToBeUpdated.setOwnBusiness([]);
    if (newRefugee.ownBusiness) {
      if (!ownBusinesses)
        newRefugee.ownBusiness.forEach(
          async (ownBusiness) =>
            await refugeeToBeUpdated.createOwnBusiness({ ...ownBusiness })
        );
      else {
        newRefugee.ownBusiness.forEach(async (ownBusiness) => {
          const ownBusinessToBeModified = await ownBusinesses.find(
            (element) => element.id == ownBusiness.id
          );
          if (ownBusinessToBeModified) {
            ownBusinessToBeModified.update({ ...ownBusiness });
            refugeeToBeUpdated.addOwnBusiness(ownBusinessToBeModified);
          } else {
            refugeeToBeUpdated.createOwnBusiness({ ...ownBusiness });
          }
        });
      }
    }
  }

  async updateRefugeeByCPF(
    cpf: string,
    refugee: Partial<Refugee>
  ): Promise<Refugee | null> {
    try {
      const refugeeToBeUpdated = await RefugeeDB.scope("formatted").findOne({
        where: { cpf },
      });
      if (!refugeeToBeUpdated) return Promise.resolve(null);
      refugeeToBeUpdated.set({
        ...refugee,
      });

      await this.updateDisabilities(refugeeToBeUpdated, refugee);
      await this.updateRole(refugeeToBeUpdated, refugee);
      await this.updateShelteringProcess(refugeeToBeUpdated, refugee);
      await this.updateCourses(refugeeToBeUpdated, refugee);
      await this.updateEducations(refugeeToBeUpdated, refugee);
      await this.updateProfessionalExperiences(refugeeToBeUpdated, refugee);
      await this.updateOwnBusiness(refugeeToBeUpdated, refugee);

      refugeeToBeUpdated.save();
      refugeeToBeUpdated.reload();

      const returnedUpdated = await RefugeeDB.scope("formatted").findOne({
        where: { cpf },
      });
      if (returnedUpdated)
        return Promise.resolve(returnedUpdated.toJSON() as Refugee);
      else return Promise.resolve(null);
    } catch (error) {
      throw error;
    }
  }

  async findUpdateOrCreateRole(role: Role): Promise<Role> {
    try {
      const [createdOrFoundRole, created] = await RoleDB.findOrCreate({
        where: {
          roleName: role.roleName,
        },
        defaults: {
          ...role,
        },
      });
      if (created) return Promise.resolve(createdOrFoundRole.toJSON());
      else {
        createdOrFoundRole.set({ ...role });
        createdOrFoundRole.save();
        return Promise.resolve(createdOrFoundRole);
      }
    } catch (error) {
      throw error;
    }
  }

  async deleteRoleById(id: number): Promise<number> {
    return RoleDB.destroy({
      where: {
        id,
      },
    });
  }

  async getAllRoles(): Promise<Role[]> {
    try {
      const query = await RoleDB.findAll();
      if (!query) return Promise.resolve([]);
      else return Promise.resolve(query.map((value) => value.toJSON()));
    } catch (error) {
      throw error;
    }
  }


  async searchRefugeeByCPF(cpf: string): Promise<RefugeeSearch[]> {
    try {
      const query = await RefugeeDB.scope('search').findAll({
        where: {
          cpf: {
            [Op.like]: `%${cpf}%`
          }
        }  
      });
      return Promise.resolve(query.map(value => value.toJSON() as RefugeeSearch));
    } catch (error) {
      throw error;
    }
  }
  async searchRefugeeByName(name: string): Promise<RefugeeSearch[]> {
    try {
      const query = await RefugeeDB.scope('search').findAll({
        where: {
          name: {
            [Op.like]: `%${name}%`
          }
        }  
      });
      return Promise.resolve(query.map(value => value.toJSON() as RefugeeSearch));
    } catch (error) {
      throw error;
    }
  }

}
