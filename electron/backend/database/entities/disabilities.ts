import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from "sequelize";

import { sequelize } from "database/index";

export class DisabilitiesDB extends Model<
  InferAttributes<DisabilitiesDB>,
  InferCreationAttributes<DisabilitiesDB>
> {
  declare id: CreationOptional<number>;

  declare whichDisabilities: string;
  declare disabilitiesMedicalReport: string;

  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;
}

DisabilitiesDB.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    whichDisabilities: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    disabilitiesMedicalReport: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    sequelize,
    tableName: "disabilities",
  }
);
