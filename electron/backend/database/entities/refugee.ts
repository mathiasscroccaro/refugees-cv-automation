import {
  CreationOptional,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyCreateAssociationMixin,
  HasManyGetAssociationsMixin,
  HasManySetAssociationsMixin,
  HasOneCreateAssociationMixin,
  HasOneGetAssociationMixin,
  HasOneSetAssociationMixin,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from "sequelize";

import { Gender } from "models/refugee";
import { sequelize } from "database/index";
import { CourseDB } from "./course";
import { DisabilitiesDB } from "./disabilities";
import { EducationDB } from "./education";
import { OwnBusinessDB } from "./own_business";
import { ProfessionalExperienceDB } from "./professional_experience";
import { RoleDB } from "./role";
import { ShelteringProcessDB } from "./shelter";
import { Role } from "@/models";

export class RefugeeDB extends Model<
  InferAttributes<RefugeeDB>,
  InferCreationAttributes<RefugeeDB>
> {
  declare id: CreationOptional<number>;
  declare name: string;
  declare dateOfBirth: Date;
  declare cpf: string;
  declare gender: string;

  declare address: CreationOptional<string>;
  declare email: CreationOptional<string>;
  declare phone: CreationOptional<string>;

  declare createCourse: HasManyCreateAssociationMixin<CourseDB>;
  declare getCourses: HasManyGetAssociationsMixin<CourseDB>;
  declare setCourses: HasManySetAssociationsMixin<CourseDB, "id">;
  declare addCourse: HasManyAddAssociationMixin<CourseDB, "id">;

  declare createEducation: HasManyCreateAssociationMixin<EducationDB>;
  declare getEducations: HasManyGetAssociationsMixin<EducationDB>;
  declare setEducations: HasManySetAssociationsMixin<EducationDB, "id">;
  declare addEducation: HasManyAddAssociationMixin<EducationDB, "id">;

  declare createProfessionalExperience: HasManyCreateAssociationMixin<ProfessionalExperienceDB>;
  declare addProfessionalExperience: HasManyAddAssociationMixin<
    ProfessionalExperienceDB,
    "id"
  >;
  declare getProfessionalExperiences: HasManyGetAssociationsMixin<ProfessionalExperienceDB>;
  declare setProfessionalExperiences: HasManySetAssociationsMixin<
    ProfessionalExperienceDB,
    "id"
  >;

  declare createOwnBusiness: HasManyCreateAssociationMixin<OwnBusinessDB>;
  declare getOwnBusiness: HasManyGetAssociationsMixin<OwnBusinessDB>;
  declare setOwnBusiness: HasManySetAssociationsMixin<OwnBusinessDB, "id">;
  declare addOwnBusiness: HasManyAddAssociationMixin<OwnBusinessDB, "id">;

  declare createDisabilities: HasOneCreateAssociationMixin<DisabilitiesDB>;
  declare getDisabilities: HasOneGetAssociationMixin<DisabilitiesDB>;

  declare createShelteringProcess: HasOneCreateAssociationMixin<ShelteringProcessDB>;
  declare getShelteringProcess: HasOneGetAssociationMixin<ShelteringProcessDB>;

  declare haveCTPS: CreationOptional<boolean>;
  declare setMainRole: HasOneSetAssociationMixin<RoleDB, "id">;
  declare professionalObjective: CreationOptional<string>;

  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;
}

RefugeeDB.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    dateOfBirth: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    cpf: {
      type: new DataTypes.STRING(11),
      allowNull: false,
      unique: true,
    },
    gender: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: { isIn: [[Gender.M, Gender.F]] },
    },
    address: {
      type: new DataTypes.STRING(128),
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: { isEmail: true },
    },
    phone: {
      type: new DataTypes.STRING(128),
      allowNull: true,
    },
    haveCTPS: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    professionalObjective: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    sequelize,
    tableName: "refugees",
  }
);
