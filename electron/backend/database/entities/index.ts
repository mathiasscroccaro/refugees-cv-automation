import { Op } from "sequelize";
import { CourseDB } from "./course";
import { DisabilitiesDB } from "./disabilities";
import { EducationDB } from "./education";
import { OwnBusinessDB } from "./own_business";
import { ProfessionalExperienceDB } from "./professional_experience";
import { RefugeeDB } from "./refugee";
import { RoleDB } from "./role";
import { ShelteringProcessDB } from "./shelter";

RefugeeDB.hasOne(DisabilitiesDB, {
  as: "disabilities",
  foreignKey: "refugeeId",
});
DisabilitiesDB.belongsTo(RefugeeDB, { foreignKey: "refugeeId" });

RefugeeDB.hasOne(ShelteringProcessDB, {
  as: "shelteringProcess",
  foreignKey: "refugeeId",
});
ShelteringProcessDB.belongsTo(RefugeeDB, { foreignKey: "refugeeId" });

RefugeeDB.belongsTo(RoleDB, { as: "mainRole", foreignKey: "mainRoleId" });
RoleDB.hasMany(RefugeeDB, { foreignKey: "mainRoleId" });

ProfessionalExperienceDB.belongsTo(RoleDB, {
  as: "role",
  foreignKey: "roleId",
});
RoleDB.hasMany(ProfessionalExperienceDB, { foreignKey: "roleId" });

RefugeeDB.hasMany(ProfessionalExperienceDB, {
  as: "professionalExperiences",
  foreignKey: "refugeeId",
});
ProfessionalExperienceDB.belongsTo(RefugeeDB, { foreignKey: "refugeeId" });

RefugeeDB.hasMany(CourseDB, { as: "courses", foreignKey: "refugeeId" });
CourseDB.belongsTo(RefugeeDB, { foreignKey: "refugeeId" });

RefugeeDB.hasMany(EducationDB, { as: "educations", foreignKey: "refugeeId" });
EducationDB.belongsTo(RefugeeDB, { foreignKey: "refugeeId" });

RefugeeDB.hasMany(OwnBusinessDB, {
  as: "ownBusiness",
  foreignKey: "refugeeId",
});
OwnBusinessDB.belongsTo(RefugeeDB, { foreignKey: "refugeeId" });

RefugeeDB.addScope("search", {
  attributes: ["name", "cpf"],
  include: [
    {
      association: "mainRole",
      attributes: {
        exclude: ["description", "createdAt", "updatedAt", "id"],
      },
    },
  ],
});

const commonAttrToBeExcluded = [
  "createdAt",
  "updatedAt",
  "refugeeId",
  "roleId",
];
const commonAttrToBeExcludedExtended = [...commonAttrToBeExcluded];

RefugeeDB.addScope("formatted", {
  include: [
    {
      association: "disabilities",
      attributes: {
        exclude: commonAttrToBeExcludedExtended,
      },
    },
    {
      association: "mainRole",
      attributes: {
        exclude: commonAttrToBeExcludedExtended,
      },
    },
    {
      association: "shelteringProcess",
      attributes: {
        exclude: commonAttrToBeExcludedExtended,
      },
    },
    {
      association: "courses",
      attributes: {
        exclude: commonAttrToBeExcluded,
      },
    },
    {
      association: "educations",
      attributes: {
        exclude: commonAttrToBeExcluded,
      },
    },
    {
      association: "professionalExperiences",
      attributes: {
        exclude: commonAttrToBeExcluded,
      },
      include: [{ association: "role" }],
    },
    {
      association: "ownBusiness",
      attributes: {
        exclude: commonAttrToBeExcluded,
      },
    },
  ],
  attributes: { exclude: ["mainRoleId"] },
});

export {
  DisabilitiesDB,
  CourseDB,
  EducationDB,
  OwnBusinessDB,
  ProfessionalExperienceDB,
  RefugeeDB,
  ShelteringProcessDB,
  RoleDB,
};
