import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from "sequelize";

import { ShelterCluster } from "models/shelter";
import { sequelize } from "database/index";

export class ShelteringProcessDB extends Model<
  InferAttributes<ShelteringProcessDB>,
  InferCreationAttributes<ShelteringProcessDB>
> {
  declare id: CreationOptional<number>;

  declare shelter: string;
  declare shelterCluster: string;
  declare internalizationProcess: boolean;
  declare internalizationDetails: CreationOptional<string>;

  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;
}

ShelteringProcessDB.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    shelter: {
      type: new DataTypes.STRING(32),
      allowNull: false,
    },
    shelterCluster: {
      type: new DataTypes.STRING(),
      allowNull: false,
      validate: {
        isIn: [
          [
            ShelterCluster.R1,
            ShelterCluster.R2,
            ShelterCluster.R5,
            ShelterCluster.AwT,
            ShelterCluster.Bv8,
          ],
        ],
      },
    },
    internalizationProcess: {
      type: new DataTypes.BOOLEAN(),
      allowNull: false,
    },
    internalizationDetails: {
      type: new DataTypes.TEXT(),
      allowNull: true,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    sequelize,
    tableName: "shelter",
  }
);
