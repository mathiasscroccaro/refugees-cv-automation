import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from "sequelize";

import { sequelize } from "database/index";

export class OwnBusinessDB extends Model<
  InferAttributes<OwnBusinessDB>,
  InferCreationAttributes<OwnBusinessDB>
> {
  declare id: CreationOptional<number>;
  declare refugeeId?: number;

  declare companyName: string;
  declare responsible: string;
  declare startAt: CreationOptional<Date>;
  declare endAt: CreationOptional<Date>;
  declare familyBusiness: boolean;
  declare employees: number;
  declare socialMedia: CreationOptional<string>;
  declare invoicing: string;
  declare description: string;
  declare localization: string;
  declare yearsOfExperience: CreationOptional<number>;
  declare currentStatus: CreationOptional<string>;

  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;
}

OwnBusinessDB.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    companyName: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    responsible: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    startAt: {
      type: new DataTypes.DATE(),
      allowNull: true,
    },
    endAt: {
      type: new DataTypes.DATE(),
      allowNull: true,
    },
    familyBusiness: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    employees: {
      type: new DataTypes.INTEGER(),
      allowNull: true,
    },
    socialMedia: {
      type: new DataTypes.STRING(256),
      allowNull: true,
    },
    invoicing: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    description: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    localization: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    yearsOfExperience: {
      type: new DataTypes.INTEGER(),
      allowNull: true,
    },
    currentStatus: {
      type: new DataTypes.TEXT(),
      allowNull: true,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    sequelize,
    tableName: "own_business",
  }
);
