import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from "sequelize";
import { EducationLevel } from "models/education";

import { sequelize } from "database/index";

export class EducationDB extends Model<
  InferAttributes<EducationDB>,
  InferCreationAttributes<EducationDB>
> {
  declare id: CreationOptional<number>;

  declare level: string;
  declare name: CreationOptional<string>;
  declare institution: string;
  declare year: number;

  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;
}

EducationDB.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    level: {
      type: new DataTypes.STRING(32),
      allowNull: false,
      validate: {
        isIn: [
          [
            EducationLevel.ELEMENTARY_SCHOOL,
            EducationLevel.HIGH_SCHOOL,
            EducationLevel.UNDERGRAD,
            EducationLevel.TECNICIAN_HIGH_SCHOOL,
            EducationLevel.INCOMPLETD_UNDERGRAD,
            EducationLevel.INCOMPLETED_ELEMENTARY_SCHOOL,
            EducationLevel.INCOMPLETED_HIGH_SCHOOL,
          ],
        ],
      },
    },
    name: {
      type: new DataTypes.STRING(256),
      allowNull: true,
    },
    institution: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    year: {
      type: new DataTypes.INTEGER(),
      allowNull: false,
      validate: {
        min: 1930,
        isInt: true,
      },
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    sequelize,
    tableName: "educations",
  }
);
