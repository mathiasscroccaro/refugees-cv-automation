import { DataTypes, Model, Optional } from "sequelize";
import { User } from "models/user";
import { sequelize } from "database/index";

type UserCreation = Optional<
  User,
  "id" | "isAdmin" | "createdAt" | "updatedAt"
>;

export class UserDB extends Model<User, UserCreation> {
  declare id: number;
  declare email: string;
  declare password: string;
  declare isAdmin: boolean;
}

UserDB.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    email: {
      type: new DataTypes.STRING(256),
      allowNull: false,
      unique: true,
    },
    password: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    isAdmin: {
      type: new DataTypes.BOOLEAN(),
      allowNull: false,
      defaultValue: false,
    },
  },
  {
    tableName: "users",
    sequelize,
  }
);
