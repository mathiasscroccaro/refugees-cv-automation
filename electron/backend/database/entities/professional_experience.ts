import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from "sequelize";

import { sequelize } from "database/index";

export class ProfessionalExperienceDB extends Model<
  InferAttributes<ProfessionalExperienceDB>,
  InferCreationAttributes<ProfessionalExperienceDB>
> {
  declare id: CreationOptional<number>;

  declare companyName: string;
  declare startAt: Date;
  declare endAt: Date;
  declare localization: string;
  declare roleDescription: string;
  declare roleId?: number;

  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;
}

ProfessionalExperienceDB.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    companyName: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    startAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    endAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    localization: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    roleDescription: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    sequelize,
    tableName: "professional_experiences",
  }
);
