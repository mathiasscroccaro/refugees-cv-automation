import {
  DataTypes,
  Model,
  InferAttributes,
  InferCreationAttributes,
  CreationOptional,
} from "sequelize";

import { sequelize } from "../index";

export class CourseDB extends Model<
  InferAttributes<CourseDB>,
  InferCreationAttributes<CourseDB>
> {
  declare id: CreationOptional<number>;
  declare refugeeId?: CreationOptional<number>;

  declare name: string;
  declare year: number;
  declare institution: string;
  declare supportedBy: string;

  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;
}

CourseDB.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    year: {
      type: new DataTypes.INTEGER(),
      allowNull: false,
    },
    institution: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    supportedBy: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    sequelize,
    tableName: "courses",
  }
);
