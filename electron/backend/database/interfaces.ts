import { User } from "models/user";
import { Refugee } from "models/refugee";
import { Role } from "models/role";
import { RefugeeSearch } from "@/models";

export abstract class IDataBase {
  abstract sync(options?: { force?: boolean }): void;

  abstract getUserByEmail(email: string): Promise<User | null>;
  abstract getUserById(id: number): Promise<User | null>;
  abstract getAllUsers(): Promise<User[]>;
  abstract createUser(user: User): Promise<User>;
  abstract modifyUserById(
    id: number,
    user: Partial<User>
  ): Promise<User | null>;
  abstract deleteUserById(id: number): Promise<number>;

  abstract createRefugee(refugee: Refugee): Promise<Refugee | null>;
  abstract updateRefugeeByCPF(
    cpf: string,
    refugee: Partial<Refugee>
  ): Promise<Refugee | null>;
  abstract getAllRefugees(): Promise<Refugee[]>;
  abstract getRefugeeByCPF(cpf: string): Promise<Refugee | null>;
  abstract deleteRefugeeByCPF(cpf: string): Promise<number>;

  abstract findUpdateOrCreateRole(role: Role): Promise<Role>;
  abstract deleteRoleById(id: number): Promise<number>;
  abstract getAllRoles(): Promise<Role[]>;

  abstract searchRefugeeByName(name: string): Promise<RefugeeSearch[]>;
  abstract searchRefugeeByCPF(cpf: string): Promise<RefugeeSearch[]>;
}
