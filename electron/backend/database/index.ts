import { GetSequelizeInstanceUseCase } from 'use_cases/sequelize';

const sequelize = new GetSequelizeInstanceUseCase().execute();

export { sequelize };

