import { Sequelize } from "sequelize";
import { BaseSQLiteDB } from "./base_db";
import { sequelize } from "./index";

export class SQLiteDB extends BaseSQLiteDB {
  sequelize: Sequelize;

  constructor() {
    super();
    this.sequelize = sequelize;
  }

  async sync(options?: { force?: boolean }) {
    await this.sequelize.sync(options);
  }
}
