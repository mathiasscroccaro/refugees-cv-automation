import express, { Request, Response, Router } from "express";
import { router as refugeesRouter } from './refugees';
import { router as rolesRouter } from './roles';
import { router as userRouter } from './users';
import { router as searchRouter } from './search';

const APIRouter: Router = express.Router();

APIRouter.get('/', (req: Request, res: Response) => {
    res.send('Refugee API working');
})
APIRouter.use("/users", userRouter);
APIRouter.use("/refugees", refugeesRouter);
APIRouter.use("/roles", rolesRouter);
APIRouter.use("/search", searchRouter);

export { APIRouter };
