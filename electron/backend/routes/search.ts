import { searchRefugeeController } from "@/controllers/search";
import express, { Router } from "express";
import { userAuthRequired } from "./middlewares";

const router: Router = express.Router();

router.post("/", userAuthRequired, searchRefugeeController);

export { router };
