import {
  createOrUpdateRoleController,
  getAllRolesController,
  deleleRoleController,
} from "controllers/roles";
import express, { Router } from "express";
import { userAuthRequired } from "./middlewares";

const router: Router = express.Router();

router.get("/", userAuthRequired, getAllRolesController);

router.post("/", userAuthRequired, createOrUpdateRoleController);

router.patch("/", userAuthRequired, createOrUpdateRoleController);

router.delete("/:id", userAuthRequired, deleleRoleController);

export { router };
