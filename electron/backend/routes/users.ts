import express, { Router } from "express";
import { userAuthRequired, adminAuthRequired } from "./middlewares";
import {
  createUserController,
  deleteUserController,
  getAllUsersController,
  getMeController,
  getTokenController,
  updateUserController,
  validateUser,
} from "controllers/users";

const router: Router = express.Router();

// / Permission = everyone
router.post("/token", getTokenController);

// Permission = user
router.get("/me", userAuthRequired, getMeController);

// Permission = Admin
router.get("/", userAuthRequired, adminAuthRequired, getAllUsersController);

// Permission = Admin
router.post(
  "/",
  userAuthRequired,
  adminAuthRequired,
  validateUser,
  createUserController
);

// Permission = Admin
router.patch("/:id", userAuthRequired, adminAuthRequired, updateUserController);

// Permission = Admin
router.delete(
  "/:id",
  userAuthRequired,
  adminAuthRequired,
  deleteUserController
);

export { router };
