import {
  createRefugeeController,
  deleteRefugeeController,
  getAllRefugeesController,
  getRefugeeController,
  updateRefugeeController,
  validateCreateRefugee,
  validateUpdateRefugee,
} from "controllers/refugees";
import express, { Router } from "express";
import { userAuthRequired, adminAuthRequired } from "./middlewares";

const router: Router = express.Router();

router.get("/", userAuthRequired, adminAuthRequired, getAllRefugeesController);

router.post(
  "/",
  userAuthRequired,
  validateCreateRefugee,
  createRefugeeController
);

router.get("/:cpf", userAuthRequired, getRefugeeController);

router.patch(
  "/:cpf",
  userAuthRequired,
  validateUpdateRefugee,
  updateRefugeeController
);

router.delete(
  "/:cpf",
  userAuthRequired,
  adminAuthRequired,
  deleteRefugeeController
);

export { router };
