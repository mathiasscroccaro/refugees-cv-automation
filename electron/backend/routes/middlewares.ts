import { NextFunction, Request, Response } from "express";
import { User } from "models/user";
import { Error403, ErrorBase } from "models/error";
import { GetAuthenticatedUserUseCase } from "use_cases/authentication";

declare global {
  namespace Express {
    interface Request {
      user?: User;
    }
  }
}

export async function adminAuthRequired(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const user = req.user;

  if (!user?.isAdmin)
    return res
      .status(403)
      .send(
        new Error403(
          `User ${user?.email} does not have admin permission`
        ).getResponse()
      );

  req.user = user;

  next();
}

export async function userAuthRequired(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const token = Array.isArray(req.headers["x-access-token"])
      ? req.headers["x-access-token"][0]
      : req.headers["x-access-token"];

    const user = await new GetAuthenticatedUserUseCase().execute(token);
    req.user = user;

    next();
  } catch (error) {
    if (error instanceof ErrorBase) {
      res.status(error.status_code);
      res.send(error.getResponse());
    } else {
      res.sendStatus(500);
    }
  }
}
